from customWidgets.ImageVisualizer import ImageVisualizer


class CliccableImageVisualizer(ImageVisualizer):
    def __init__(self, image_name, callback, master=None):
        ImageVisualizer.__init__(self, image_name, master)
        self.image_canvas.bind("<Button-1>", callback)

    def bind(self, event, callback):
        self.image_canvas.bind(event, callback)