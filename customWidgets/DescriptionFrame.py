import tkinter as tk

class DescriptionFrame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None, title=None, description=None):
        tk.LabelFrame.__init__(self, master, text = "Description")
        self.rowconfigure(0, weight = 1)
        self.rowconfigure(1, weight = 99)
        self.grid_propagate(0)
        self.grid(sticky="nswe")

        if title is None:
            self.title = ""
        else:
            self.title=title
        if description is None:
            self.description = ""
        else:
            self.description=description
        self.titleLabel = tk.Label(self, text = self.title, font="Helvetica 12 bold")
        self.titleLabel.grid(row=0, sticky="n")
        self.descriptionLabel = tk.Label(self, text = self.description, font="Helvetica 8")
        self.descriptionLabel.grid(row=1, sticky="nw")
        self.descriptionLabel.configure(wraplength=self.winfo_width())

