import tkinter as tk
from tkinter import ttk

class TreeviewWithScrollbar(tk.LabelFrame):
    """description of class"""

    def __init__(self, doubleClickMethod, master=None):
        if doubleClickMethod is None:
            raise ValueError("doubleClickMethod must not be None")

        self.implemented_homeworks = ['Homework 2', 'Homework 3', 'Homework 4', 'Homework 5', 'Homework 6']
        self.implemented_labs = ['Laboratorio 2', 'Laboratorio 3','Laboratorio 5', 'Laboratorio 7', 'Laboratorio 10', 'Laboratorio 11']
        self.labelFrameName = "Esame di Visione Artificiale"
        self.doubleClickMethod = doubleClickMethod
        tk.LabelFrame.__init__(self, master, text = self.labelFrameName, padx = 5, pady = 5)
        self.grid()
        self.createWidgets()

    def createWidgets(self):
        self.treeView = ttk.Treeview(self, selectmode = "browse")
        self.treeView.heading("#0", text = "ciao")
        topLevelIid = self.treeView.insert("", 0, text = "Esame Visione Artificiale", open = True)
        homeworksIid = self.treeView.insert(topLevelIid, "end", text = "Homeworks", open = True)
        for homework in self.implemented_homeworks:            
            self.treeView.insert(homeworksIid, "end", text = homework)
        
        labIid = self.treeView.insert(topLevelIid, "end", text = "Laboratorio", open = True)
        for lab in self.implemented_labs:            
            self.treeView.insert(labIid, "end", text = lab)
        
        self.treeView.bind("<Double-1>", self.doubleClickMethod)
        self.treeView.grid(row = 0, column = 0, sticky = "news")

        self.treeViewScrollBar = ttk.Scrollbar(self)
        self.treeViewScrollBar.grid(row = 0, column = 1, sticky="NS")
        self.treeView.configure(yscrollcommand=self.treeViewScrollBar.set)

        self.columnconfigure(0, weight = 1)
        self.columnconfigure(1, weight = 1)
        self.rowconfigure(0, weight = 1)
