import tkinter as tk
from PIL import Image, ImageTk
import cv2

class ResizingCanvas(tk.Canvas):
    def __init__(self, master = None, cnf = {}, **kw):
        tk.Canvas.__init__(self, master, cnf, **kw)
        self.bind("<Configure>", self.on_resize)
        self.height = self.winfo_reqheight()
        self.width = self.winfo_reqwidth()
        self.ipadx = 3
        self.ipady = 3

    def on_resize(self,event):
        if not hasattr(self, "image"):
            return
        new_shape = self.get_new_image_shape(self.image)        
        resized_image_tk = self.from_rgb_to_resized_tk_image(self.image)
        self.dx = self.winfo_width() / 2 - resized_image_tk.width() / 2
        self.dy = self.winfo_height() / 2 - resized_image_tk.height() / 2
        self.create_image(self.dx, self.dy, image = resized_image_tk, anchor = "nw")
        ##The following line is necessary to prevent the image from being garbage collected
        self.tk_image = resized_image_tk

    def has_image(self):
        return hasattr(self, "image") and not self.image is None

    def get_image_position(self):
        return self.dx, self.dy    

    def delete_image(self):
        if hasattr(self, "image_id") and not self.image_id is None:
            self.delete(self.image_id)
            self.image_id = None
        if hasattr(self, "error_text") and not self.error_text is None:
            self.delete(self.error_text_id)
            self.error_text = None


    def set_image(self, rgb_image):
        if hasattr(self, "error_text") and not self.error_text is None:
            self.delete(self.error_text_id)
            self.error_text = None
        self.image = rgb_image
        tk_image = self.from_rgb_to_resized_tk_image(rgb_image)
        self.dx = self.winfo_width() / 2 - tk_image.width() / 2
        self.dy = self.winfo_height() / 2 - tk_image.height() / 2
        self.image_id = self.create_image(self.dx, self.dy, image = tk_image, anchor = "nw")
        ##The following line is necessary to prevent the image from being garbage collected
        self.tk_image = tk_image

    def set_error_message(self, text):
        if hasattr(self, "image_id") and not self.image_id is None:
            self.delete(self.image_id)
            self.image_id = None
        self.error_text = text
        self.error_text_id = self.create_text(20, 30, text=self.error_text, anchor="nw")

    def from_rgb_to_resized_tk_image(self, rgb_image):                
        new_shape = self.get_new_image_shape(rgb_image)
        resized_rgb_image = cv2.resize(rgb_image, new_shape)
        resized_pil_image = Image.fromarray(resized_rgb_image)
        return ImageTk.PhotoImage(resized_pil_image)

    def get_new_image_shape(self, rgb_image):
        canvas_width = self.winfo_width() - self.ipadx
        canvas_height = self.winfo_height() - self.ipady
        width_ratio = canvas_width / float(rgb_image.shape[1])
        height_ratio = canvas_height / float(rgb_image.shape[0])
        if (height_ratio > width_ratio):
            self.ratio = width_ratio
        else:
            self.ratio = height_ratio

        new_rgb_image_width = self.ratio * rgb_image.shape[1]
        new_rgb_image_height = self.ratio * rgb_image.shape[0]

        return int(new_rgb_image_width), int(new_rgb_image_height)

    def get_current_ratio(self):
        if hasattr(self, 'ratio') and not self.ratio is None:
            return self.ratio
        else:
            return -1