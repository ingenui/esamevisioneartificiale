import cv2
import numpy as np
import tkinter as tk
from tkinter import filedialog
from Homeworks.Homework3 import canny, ghirlandina_template_match, spectral_clustering
from customWidgets import ImageVisualizer

class Homework3Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Homework 3", padx = 5, pady = 5)
        self.columnconfigure(0, weight=12)
        self.columnconfigure(1, weight=12)
        self.columnconfigure(2, weight=12)
        self.columnconfigure(3, weight=12)
        self.columnconfigure(4, weight=50)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)
        self.rowconfigure(3, weight=1)
        self.rowconfigure(4, weight=96)

        self.original_image_path = ""

        self.grid_propagate(0)
        self.grid(sticky="nswe")
        self.createWidgets()


    def createWidgets(self):
        self.choose_original_image_button = tk.Button(self, text = "Choose file", command = self.choose_original_image_path)
        self.choose_original_image_button.grid(row = 0, column = 0, columnspan = 2)

        self.choose_original_image_button = tk.Button(self, text = "GO!", command = self.button_pressed)
        self.choose_original_image_button.grid(row = 0, column = 2, columnspan = 2)
        self.choose_original_image_button.config(state = tk.DISABLED)

        self.canny_low_threshold = tk.Scale(self, from_=1, to=255, orient=tk.HORIZONTAL, label = "THL")
        self.canny_low_threshold.grid(row=1, column=0, columnspan = 2)

        self.canny_high_threshold = tk.Scale(self, from_=1, to=255, orient=tk.HORIZONTAL, label = "THH")
        self.canny_high_threshold.grid(row=1, column=1, columnspan = 2)
        
        self.alpha_coefficient = tk.Scale(self, from_=1, to=10, orient=tk.HORIZONTAL, label = "Alpha", command = self.update_segmented_image_blended_with_original)
        self.alpha_coefficient.grid(row=2, column=0, columnspan = 2)

        self.canny_config_mode_label = tk.Label(self, text="Mode:").grid(row=2, column=2)
        self.canny_config_mode_text = tk.StringVar()        
        self.canny_config_mode_text_label = tk.Label(self, textvariable=self.canny_config_mode_text)
        self.canny_config_mode_text_label.grid(row=2, column=3)        

        self.method = tk.IntVar()
        self.select_method_canny = tk.Radiobutton(self, text="Canny", variable=self.method, value=1)
        self.select_method_canny.grid(row=3, column=0)
        self.select_method_kmeans = tk.Radiobutton(self, text="Kmeans", variable=self.method, value=0)
        self.select_method_kmeans.grid(row=3, column=1)

        self.original_image_visualizer = ImageVisualizer.ImageVisualizer("Original", self)
        self.original_image_visualizer.grid(row=4, column=0, padx=3, pady=3, columnspan = 4)

        self.segmented_image_visualizer = ImageVisualizer.ImageVisualizer("Segmented", self)
        self.segmented_image_visualizer.grid(row=4, column=4, padx=3, pady=3)

    def choose_original_image_path(self):
        self.original_image_path = filedialog.askopenfilename()
        if self.original_image_path == "":
            return
        self.choose_original_image_button.config(state = tk.NORMAL)
        self.original_image = cv2.cvtColor(cv2.imread(self.original_image_path, cv2.IMREAD_COLOR), cv2.COLOR_RGB2BGR)
        self.original_image_visualizer.set_image(self.original_image)

    def button_pressed(self):
        if not hasattr(self, "original_image") or self.original_image is None:
            return 0
        self.canny_lt, self.canny_ht, self.canny_mode = canny.findCannyConfiguration(self.original_image.astype(np.uint8))
        self.canny_low_threshold.set(self.canny_lt)
        self.canny_high_threshold.set(self.canny_ht)
        self.canny_config_mode_text.set(self.canny_mode)

        self.img_segmented = self.get_segmented_image()
        self.update_segmented_image_blended_with_original(self.alpha_coefficient.get())
        

    def get_segmented_image(self):    
        if not hasattr(self, "original_image") or self.original_image is None:
            return 0
        
        grayscale_image = cv2.cvtColor(self.original_image, cv2.COLOR_BGR2GRAY)

        if(self.method.get()==0):            
            self.img_segmented = spectral_clustering.main_kmeans_clustering_image(grayscale_image)
            self.metodo = 'K-means'
        else:
            self.img_segmented = canny.cannySeparate(self.original_image, self.canny_low_threshold.get(), self.canny_high_threshold.get())
            self.metodo = 'Canny'

        return ghirlandina_template_match.ghirlandina_match(grayscale_image.astype(np.uint8), self.img_segmented.astype(np.uint8))
        

    def update_segmented_image_blended_with_original(self, value):
        if not hasattr(self, "original_image") or self.original_image is None:
            return 0
        alpha=np.array([self.alpha_coefficient.get()/10., self.alpha_coefficient.get()/10., self.alpha_coefficient.get()/10.])
        out_img = self.original_image * alpha
        out_img += self.label_to_color(self.img_segmented) * (1. - alpha)
        out_img = out_img.astype(np.uint8)
        self.segmented_image_visualizer.set_image(out_img)

    def label_to_color(self, img):        
        map={
            0: (0,0,0),
            1: (255, 0, 255),
            2: (0, 255, 0)
        }
        imgc = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        imgc[img == 0] = map[0]
        imgc[img == 1] = map[1]
        imgc[img == 2] = map[2]
        return imgc