import threading

class ThreadedNetLoaderBase(threading.Thread):
    def __init__(self, queue, homework6):
        threading.Thread.__init__(self)
        self.homework6 = homework6
        self.queue = queue

    def get_detector_id(self):
        raise NotImplementedError("ThreadedNetLoaderBase is an abstract class")

    def run(self):
        self.homework6.load_detector(self.get_detector_id())
        self.queue.put("finished")
