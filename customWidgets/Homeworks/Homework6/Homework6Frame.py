import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import os
from customWidgets import ImageVisualizer
from Homeworks.Homework6 import homework6
from customWidgets.Homeworks.Homework6.ThreadedFrameUpdater import ThreadedFrameUpdater
from customWidgets.Homeworks.Homework6 import ThreadedNetLoaderFactory
import time
import queue

class Homework6Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Homework 6", padx = 5, pady = 5)
        self.columnconfigure(0, weight = 1)
        self.columnconfigure(1, weight = 1)
        self.columnconfigure(2, weight = 1)
        self.columnconfigure(3, weight = 1)
        self.columnconfigure(4, weight = 1)
        self.columnconfigure(5, weight = 1)
        self.columnconfigure(6, weight = 1)
        self.columnconfigure(7, weight = 1)
        self.rowconfigure(0, weight = 1)
        self.rowconfigure(1, weight = 1)
        self.rowconfigure(2, weight = 1)
        self.rowconfigure(3, weight = 97)

        self.grid_propagate(0)
        self.grid(sticky = "nswe")
        self.createWidgets()
        self.homework6 = homework6.Homework6()
        self.video_started = False
        self.reload_net = True

    def createWidgets(self):
        self.start_stop_button = tk.Button(self, text = "Start", command = self.start_stop_video)
        self.start_stop_button.grid(row = 0, column = 0, sticky = "n", rowspan = 2)

        #self.fps_label = tk.Label(self, text = "fps")
        #self.fps_label.grid(row = 0, column = 1, rowspan = 2)        

        self.detector_label = tk.Label(self, text="Detector not ready", fg="red")
        self.detector_label.grid(row = 2, column = 1)

        self.select_model_button = tk.Button(self, text="Select model", command=self.select_model_path)
        self.select_model_button.grid(row=0, column=2, sticky="n", rowspan = 2)

        self.model_path_label = tk.Label(self, text = "Default model selected")
        self.model_path_label.grid(row = 2, column = 2)
        
        self.select_weights_button = tk.Button(self, text="Select weights", command=self.select_weights_path)
        self.select_weights_button.grid(row=0, column = 3, sticky="n", rowspan = 2)

        self.weights_path_label = tk.Label(self, text = "Default weights selected")
        self.weights_path_label.grid(row = 2, column = 3)

        self.video_frame_visualizer = ImageVisualizer.ImageVisualizer("Video", self)
        self.video_frame_visualizer.grid(row = 3, column = 0, padx = 3, pady = 3, columnspan = 7)

        self.radiobutton_value = tk.IntVar()
        self.rb_lab_camera_video_source = tk.Radiobutton(self, text = "Lab camera", variable = self.radiobutton_value, value = 1, command = self.video_source_changed)
        self.rb_lab_camera_video_source.select()
        self.rb_lab_camera_video_source.grid(row = 0, column = 4)        
        self.rb_local_video_source = tk.Radiobutton(self, text = "Choose video", variable = self.radiobutton_value, value = 2, command = self.video_source_changed)
        self.rb_local_video_source.grid(row = 1, column = 4)
        self.rb_local_frames_source = tk.Radiobutton(self, text="Choose frames folder", variable = self.radiobutton_value, value = 3, command = self.video_source_changed)
        self.rb_local_frames_source.grid(row=2, column=4)

        self.choose_local_video_button = tk.Button(self, text = "Choose video", command = self.choose_local_video)
        self.choose_local_video_button.grid(row = 0, column = 5, rowspan = 2, sticky = "n")
        self.choose_local_video_button.config(state = tk.DISABLED)

        self.chosen_video_name = tk.Label(self, text = "Video name")        
        self.chosen_video_name.grid(row = 2, column = 5)
        
        self.supported_algorithms = [ThreadedNetLoaderFactory.algorithm_YOLO, 
                                     ThreadedNetLoaderFactory.algorithm_YOLO_Sort]
        self.current_algorithm = ThreadedNetLoaderFactory.algorithm_YOLO_Sort
        self.algorithm_combo_box = ttk.Combobox(self, values = self.supported_algorithms)
        self.algorithm_combo_box.set(self.current_algorithm)
        self.algorithm_combo_box.bind("<<ComboboxSelected>>", self.change_current_algorithm)
        self.algorithm_combo_box.grid(row = 0, column = 6, rowspan = 2, sticky = "n")
       
        self.chosen_labels_directory = tk.Label(self, text ="Labels directory")
        self.chosen_labels_directory.grid(row = 2, column = 7)

        self.select_labels_button = tk.Button(self, text="Select solution", command=self.select_solution)
        self.select_labels_button.grid(row=0, column=7, sticky="n")
        self.select_labels_button.config(state=tk.DISABLED)

    def change_current_algorithm(self, event):
        self.current_algorithm = self.algorithm_combo_box.get()
        self.reload_net = True

    def choose_local_video(self):
        if self.radiobutton_value.get() == 3:
            local_frames = filedialog.askdirectory()
            self.chosen_video_name.config(text = os.path.basename(local_frames))
            self.homework6.set_video_source(local_frames)
        else:
            local_video = filedialog.askopenfilename()
            self.chosen_video_name.config(text = os.path.basename(local_video))
            self.homework6.set_video_source(local_video)

    def video_source_changed(self):
        if self.radiobutton_value.get() == 1:
            self.homework6.set_video_source("lab")
            self.choose_local_video_button.config(state = tk.DISABLED)
            self.select_labels_button.config(state=tk.DISABLED)
            self.homework6.labels_path = None
            self.homework6.labels = None
        elif self.radiobutton_value.get() == 3:
            self.select_labels_button.config(state = tk.NORMAL)
            self.choose_local_video_button.config(state = tk.NORMAL, text='Choose directory')
        else:
            self.homework6.labels_path = None
            self.homework6.labels = None
            self.select_labels_button.config(state=tk.DISABLED)
            self.choose_local_video_button.config(state=tk.NORMAL, text='Chose video')


    def select_model_path(self):
        model_path = filedialog.askopenfilename()
        self.homework6.set_model_path(model_path)
        self.model_path_label.config(text=os.path.basename(self.homework6.model_path))
        self.reload_net = True

    def select_weights_path(self):
        weights_path = filedialog.askopenfilename()
        self.homework6.set_weights_path(weights_path)
        self.weights_path_label.config(text=os.path.basename(self.homework6.weights_path))
        self.reload_net = True

    def select_solution(self):
        labels_path = filedialog.askopenfilename()
        self.homework6.set_labels_path(labels_path)
        self.chosen_labels_directory.config(text=os.path.basename(labels_path))

    def select_threshold(self):
        pass

    def start_stop_video(self):
        if self.video_started:
            self.homework6.stop_video_capture()
            self.start_stop_button.config(text = "Start")
            self.rb_lab_camera_video_source.config(state = tk.NORMAL)
            self.rb_local_video_source.config(state = tk.NORMAL)
            self.rb_local_frames_source.config(state = tk.NORMAL)
            self.select_model_button.config(state = tk.NORMAL)
            self.select_weights_button.config(state = tk.NORMAL)
            self.select_labels_button.config(state = tk.NORMAL)
            self.choose_local_video_button.config(state = tk.NORMAL)
            self.algorithm_combo_box.config(state = tk.NORMAL)
            self.updater.stop()
        else:
            self.updater = ThreadedFrameUpdater(self.update_video)
            #Caricamento di TFNet e procedura di laoding
            self.detector_loader_queue = queue.Queue()            
            self.rb_lab_camera_video_source.config(state = tk.DISABLED)
            self.rb_local_video_source.config(state = tk.DISABLED)
            self.rb_local_frames_source.config(state = tk.DISABLED)
            self.select_model_button.config(state = tk.DISABLED)
            self.select_weights_button.config(state = tk.DISABLED)
            self.select_labels_button.config(state = tk.DISABLED)
            self.choose_local_video_button.config(state = tk.DISABLED)
            self.algorithm_combo_box.config(state = tk.DISABLED)
            
            if self.reload_net:
                self.start_progress_bar()
                self.start_stop_button.config(state=tk.DISABLED)
                self.detector_label.config(text="Loading detector", fg="blue")
                ThreadedNetLoaderFactory.ThreadedNetLoaderFactory.get_instance(self.current_algorithm, 
                                                                               self.detector_loader_queue, 
                                                                               self.homework6).start()                
                self.master.after(100, self.process_detector_loader_queue)
                self.reload_net = False
            else:
                self.start_capture()

        self.video_started = not self.video_started

    def process_detector_loader_queue(self):
        try:
            #Estrazione dalla queue per controllare se il loading è terminato
            self.detector_loader_queue.get(0)
            #Estrazione delnome del detector da stampare
            detector_name = self.homework6.detector.name
            if not self.homework6.detector.version is None:
                detector_name += " " + self.homework6.detector.version
            self.detector_label.config(fg="green", text=detector_name + " ready")
            self.start_capture()
            self.progress_bar.stop()
            self.progress_bar.destroy()
        except queue.Empty:
            self.master.after(100, self.process_detector_loader_queue)

    def start_capture(self):
        #Parte il processo di video_capture (e detection) in homework6
        self.homework6.start_video_capture()
        self.start_stop_button.config(text="Stop", state=tk.NORMAL)
        #Parte il processo del consumatore di frame pronti per essere visualizzati
        self.updater.start()

    def start_progress_bar(self):
        self.progress_bar = ttk.Progressbar(self, orient = "horizontal", mode = "indeterminate")
        self.progress_bar.grid(row = 1, column = 0, sticky = "we", columnspan = 1)
        self.progress_bar.start()


    def update_video(self):        
        next_frame = self.homework6.get_next_frame()
        if not next_frame is None:
            start_time = time.time()
            self.video_frame_visualizer.set_image(next_frame)
            #self.fps_label.config(text = "{} fps".format(int((1. / (time.time() - start_time)))))