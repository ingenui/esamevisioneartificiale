from customWidgets.Homeworks.Homework6.ThreadedLoaders import ThreadedHumanDetectorNetLoader, ThreadedTrackerNetLoader

algorithm_YOLO = "YOLO"
algorithm_YOLO_Sort = "YOLO + Sort"

class ThreadedNetLoaderFactory(object):
    """description of class"""

    @staticmethod
    def get_instance(algorithm, queue, homework):
        if algorithm == algorithm_YOLO:
            return ThreadedHumanDetectorNetLoader.ThreadedHumanDetectorNetLoader(queue, homework)
        elif algorithm == algorithm_YOLO_Sort:
            return ThreadedTrackerNetLoader.ThreadedTrackerNetLoader(queue, homework)
        else:
            raise ValueError()


