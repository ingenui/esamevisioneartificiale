import tkinter as tk
import cv2
from tkinter import filedialog
from Homeworks.Homework4 import homework4 as hw
from customWidgets import ImageVisualizer
from customWidgets.Homeworks.Homework4.Segmentation import SegmentationFrame
from customWidgets.Homeworks.Homework4 import PipelineBlendFame, MorphologyFrame, ConnectedComponentsFrame, BoundingBoxesTreeview



class Homework4Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text="Homework 4", padx=5, pady=5)

        self.drawn_bounding_boxes = []

        self.columnconfigure(0, weight=5)
        self.columnconfigure(1, weight=5)
        self.columnconfigure(2, weight=30)
        self.columnconfigure(3, weight=30)
        self.columnconfigure(4, weight=30)       
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=42)
        self.rowconfigure(2, weight=2)
        self.rowconfigure(3, weight=55)

        self.grid_propagate(0)
        self.grid(sticky="nswe")        
        self.hw4 = hw.homework4()
        self.createWidgets()
        

    def createWidgets(self):
        self.chooseFileButton = tk.Button(self, text="Choose file", command=self.choose_original_image_path)
        self.chooseFileButton.grid(row=0, column=0, sticky="n")

        self.chooseFileButton = tk.Button(self, text="Run the pipeline!", command=self.pipeline)
        self.chooseFileButton.config(state = tk.DISABLED)
        self.chooseFileButton.grid(row=0, column=1, sticky="n")

        self.original_image_visualizer = ImageVisualizer.ImageVisualizer("Original", self)
        self.original_image_visualizer.grid(row=1, column=0, padx=3, pady=3, columnspan = 2)

        self.final_image_visualizer = ImageVisualizer.ImageVisualizer("Final image", self)
        self.final_image_visualizer.grid(row=2, column=0, padx=3, pady=3, columnspan=4, rowspan = 2)

        self.segmentation_frame = SegmentationFrame.SegmentationFrame(self)
        self.segmentation_frame.grid(row = 0, column = 2, rowspan = 2)
        
        self.morphology_frame = MorphologyFrame.MorphologyFrame(self)
        self.morphology_frame.grid(row = 0, column = 3, rowspan = 2)

        self.connected_components_frame = ConnectedComponentsFrame.ConnectedComponentsFrame(self)
        self.connected_components_frame.grid(row = 0, column = 4, rowspan = 2)

        self.bounding_boxes_treeview = BoundingBoxesTreeview.BoundingBoxesTreeview(self.click_on_object, self)
        self.bounding_boxes_treeview.grid(row=2, rowspan=1, column=4, sticky="nwes")

        self.pipeline_blend_frame = PipelineBlendFame.PipelineBlendFrame(self.hw4, self)
        self.pipeline_blend_frame.grid(row = 3, column = 4)


    def choose_original_image_path(self):
        self.original_image_path = filedialog.askopenfilename()
        if self.original_image_path == "":
            return
        self.original_image = cv2.cvtColor(cv2.imread(self.original_image_path, cv2.IMREAD_COLOR), cv2.COLOR_RGB2BGR)
        self.original_image_visualizer.set_image(self.original_image)        
        self.chooseFileButton.config(state = tk.NORMAL)
        self.pipeline_blend_frame.enable()
        self.hw4.set_original_image(self.original_image)
        self.segmentation_frame.segmentation_visualizer.image_canvas.delete_image()
        self.morphology_frame.morphology_visualizer.image_canvas.delete_image()
        self.connected_components_frame.connected_components_visualizer.image_canvas.delete_image()
        self.final_image_visualizer.image_canvas.delete_image()
        self.pipeline_blend_frame.blend_visualizer.image_canvas.delete_image()
        self.delete_all_drawn_bounding_boxes()
        self.bounding_boxes_treeview.clear()
        self.previous_selected_object_name = None

    def click_on_object(self, object_name):
        #delete previous selected object if any
        if hasattr(self, "previous_selected_object_name") and not self.previous_selected_object_name is None:
            previous_selected_object = self.hw4.get_found_object_by_name(self.previous_selected_object_name)
            previous_selected_object_bbox = self.get_drawn_bounding_box_by_object(previous_selected_object)
            self.delete_drawn_bounding_box(previous_selected_object_bbox)
            self.draw_bounding_box_on_final_image_canvas(previous_selected_object)

        #draw thicker bounding box
        selected_object = self.hw4.get_found_object_by_name(object_name)
        bbox = self.get_drawn_bounding_box_by_object(selected_object)
        self.delete_drawn_bounding_box(bbox)
        self.draw_bounding_box_on_final_image_canvas(selected_object, 3)
        self.previous_selected_object_name = object_name

    def get_drawn_bounding_box_by_object(self, found_object):
        for drawn_bbox in self.drawn_bounding_boxes:
            bbox_name = drawn_bbox[0]
            if bbox_name == found_object.name:
                return drawn_bbox

    def get_canvas_coordinates_from_image_coordinates(self, image_coords):
        dx, dy = self.final_image_visualizer.image_canvas.get_image_position()
        x = int(image_coords[0] * self.final_image_visualizer.image_canvas.get_current_ratio() + dx)
        y = int(image_coords[1] * self.final_image_visualizer.image_canvas.get_current_ratio() + dy)
        return x, y

    def delete_drawn_bounding_box(self, drawn_bbox):        
        self.final_image_visualizer.image_canvas.delete(drawn_bbox[1])
        self.final_image_visualizer.image_canvas.delete(drawn_bbox[2])
        self.final_image_visualizer.image_canvas.delete(drawn_bbox[3])
        self.final_image_visualizer.image_canvas.delete(drawn_bbox[4])
        self.drawn_bounding_boxes.remove(drawn_bbox)

    def draw_bounding_box_on_final_image_canvas(self, found_object, line_width = 1):
        bbox = found_object.bounding_box
        x0, y0 = self.get_canvas_coordinates_from_image_coordinates((bbox[0][0], bbox[0][1]))
        x1, y1 = self.get_canvas_coordinates_from_image_coordinates((bbox[1][0], bbox[1][1]))
        x2, y2 = self.get_canvas_coordinates_from_image_coordinates((bbox[2][0], bbox[2][1]))
        x3, y3 = self.get_canvas_coordinates_from_image_coordinates((bbox[3][0], bbox[3][1]))
        line1 = self.final_image_visualizer.image_canvas.create_line(x0, y0, x1, y1, fill = "red", width = line_width)
        line2 = self.final_image_visualizer.image_canvas.create_line(x1, y1, x2, y2, fill = "red", width = line_width)
        line3 = self.final_image_visualizer.image_canvas.create_line(x2, y2, x3, y3, fill = "red", width = line_width)
        line4 = self.final_image_visualizer.image_canvas.create_line(x3, y3, x0, y0, fill = "red", width = line_width)
        self.drawn_bounding_boxes.append((found_object.name, line1, line2, line3, line4))

    def delete_all_drawn_bounding_boxes(self):    
        tot_drawn_boxes = len(self.drawn_bounding_boxes)
        for i in range(tot_drawn_boxes):
            #always remove from the head of underlying list
            self.delete_drawn_bounding_box(self.drawn_bounding_boxes[0])

    def draw_bounding_boxes(self, found_objects):
        self.previous_selected_object_name = None

        self.delete_all_drawn_bounding_boxes()

        for object in found_objects:
            self.draw_bounding_box_on_final_image_canvas(object)
            

    def pipeline(self):
        self.final_image = self.original_image
        ################
        ### PIPELINE ###
        ################
        window_size = self.segmentation_frame.get_window_size()
        c_constant = self.segmentation_frame.get_c_constant()
        adaptive_threshold_method = self.segmentation_frame.get_adaptive_threshold_method()
        open_kernel_size = self.morphology_frame.get_open_kernel_size()
        close_kernel_size = self.morphology_frame.get_close_kernel_size()
        open_check = self.morphology_frame.get_open_check()
        close_check = self.morphology_frame.get_close_check()
        connectivity = self.connected_components_frame.get_connectivity()
        percentage_threshold = self.connected_components_frame.get_percentage_threshold()

        self.final_image = self.hw4.run_pipeline(window_size, c_constant, adaptive_threshold_method, open_kernel_size, close_kernel_size, 
                                                 open_check, close_check, connectivity, percentage_threshold)

        self.segmentation_frame.set_segmentation_image(self.hw4.get_segmented_image())
        self.morphology_frame.set_morphology_image(self.hw4.get_morphology_image())
        self.connected_components_frame.set_connected_components_image(self.hw4.get_connected_components_image())
        self.bounding_boxes_treeview.add_found_objects(self.hw4.get_found_objects())
        self.final_image_visualizer.set_image(self.hw4.get_original_image())
        self.draw_bounding_boxes(self.hw4.get_found_objects())

