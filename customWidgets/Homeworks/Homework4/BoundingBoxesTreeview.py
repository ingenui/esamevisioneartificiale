import tkinter as tk
from tkinter import ttk

class BoundingBoxesTreeview(tk.LabelFrame):
    """description of class"""

    def __init__(self, object_selected_callback, master=None):        
        self.labelFrameName = "Oggetti trovati"
        self.object_selected_callback = object_selected_callback
        tk.LabelFrame.__init__(self, master, text=self.labelFrameName, padx=5, pady=5)
        self.grid(sticky="nswe")
        self.createWidgets()

    def double_click_method(self, event):
        item = self.treeView.selection()[0]
        itemText = self.treeView.item(item, "text")
        self.object_selected_callback(itemText)

    def clear(self):
        self.treeView.delete(*self.treeView.get_children())

    def add_found_objects(self, found_objects):
        if found_objects is None:
            return

        self.clear()

        for object in found_objects:
            self.treeView.insert('', "end", text = object.get_name())

    def createWidgets(self):
        self.treeView = ttk.Treeview(self, selectmode="browse")
        self.treeView.heading("#0")

        self.treeView.bind("<Double-1>", self.double_click_method)
        self.treeView.grid(row=0, column=0, sticky="news")

        self.treeViewScrollBar = ttk.Scrollbar(self)
        self.treeViewScrollBar.grid(row=0, column=1, sticky="nse")
        self.treeView.configure(yscrollcommand=self.treeViewScrollBar.set)

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)
