import tkinter as tk
from customWidgets import ImageVisualizer


class MorphologyFrame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text="Morphology", padx=5, pady=5)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=1)
        self.rowconfigure(0, weight=91)
        self.rowconfigure(1, weight=3)
        self.rowconfigure(2, weight=3)
        self.rowconfigure(3, weight=3)
       
        self.grid_propagate(0)
        self.grid(sticky="nswe")
        self.createWidgets()        

    def createWidgets(self):
        self.morphology_visualizer = ImageVisualizer.ImageVisualizer("Open - close", self)
        self.morphology_visualizer.grid(row=0, column=0, padx=3, pady=3, columnspan = 3)

        self.open_check_var = tk.IntVar()
        self.open_check = tk.Checkbutton(self, variable = self.open_check_var)
        self.open_check.grid(row = 1, column = 0)

        self.open_kernel_size_label = tk.Label(self, text = "Open kernel size")
        self.open_kernel_size_label.grid(row = 1, column = 1)

        self.open_kernel_size_var = tk.StringVar()
        self.open_kernel_size_spinbox = tk.Spinbox(self, from_ = 1, to = 50, state = 'readonly', textvariable = self.open_kernel_size_var)
        self.previous_open_kernel_size_value = 3
        self.open_kernel_size_var.set(str(self.previous_open_kernel_size_value))
        self.open_kernel_size_spinbox.grid(row = 1, column = 2)

        self.close_check_var = tk.IntVar()
        self.close_check = tk.Checkbutton(self, variable = self.close_check_var)
        self.close_check_var.set(1)
        self.close_check.grid(row = 2, column = 0)
        
        self.close_kernel_size_label = tk.Label(self, text = "Close kernel size")
        self.close_kernel_size_label.grid(row = 2, column = 1)

        self.close_kernel_size_var = tk.StringVar()
        self.close_kernel_size_spinbox = tk.Spinbox(self, from_ = 1, to = 50, state = 'readonly', textvariable = self.close_kernel_size_var)
        self.previous_close_kernel_size_value = 3
        self.close_kernel_size_var.set(str(self.previous_close_kernel_size_value))
        self.close_kernel_size_spinbox.grid(row = 2, column = 2)
        

    def get_open_kernel_size(self):
        return int(self.open_kernel_size_var.get())

    def get_close_kernel_size(self):
        return int(self.close_kernel_size_var.get())

    def get_open_check(self):
        return self.open_check_var.get()

    def get_close_check(self):
        return self.close_check_var.get()
    
    def set_morphology_image(self, rgb_image):
        self.morphology_visualizer.image_canvas.set_image(rgb_image)
