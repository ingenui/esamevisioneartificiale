import tkinter as tk
from customWidgets import ImageVisualizer


class SegmentationFrame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text="Segmentation", padx=5, pady=5)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=91)
        self.rowconfigure(1, weight=3)
        self.rowconfigure(2, weight=3)
        self.rowconfigure(3, weight=3)
       
        self.grid_propagate(0)
        self.grid(sticky="nswe")
        self.createWidgets()        

    def createWidgets(self):
        self.segmentation_visualizer = ImageVisualizer.ImageVisualizer("Adaptive Threshold", self)
        self.segmentation_visualizer.grid(row=0, column=0, padx=3, pady=3, columnspan = 2)

        self.window_size_label = tk.Label(self, text = "Window size")
        self.window_size_label.grid(row = 1, column = 0)

        self.window_size_var = tk.StringVar()
        self.window_size_spinbox = tk.Spinbox(self, from_ = 3, to = 31, command = self.force_only_odd_values, 
                                              state = 'readonly', textvariable = self.window_size_var)
        self.previous_window_size_value = 13
        self.window_size_var.set(str(self.previous_window_size_value))
        self.window_size_spinbox.grid(row = 1, column = 1)
        
        self.c_constant_label = tk.Label(self, text = "C constant")
        self.c_constant_label.grid(row = 2, column = 0)

        self.c_constant_var = tk.StringVar()
        self.c_constant_spinbox = tk.Spinbox(self, from_ = 1, to = 40, state = 'readonly', textvariable = self.c_constant_var)        
        self.c_constant_var.set(str(3))
        self.c_constant_spinbox.grid(row = 2, column = 1)

        self.adaptive_method_var = tk.IntVar()
        self.mean_method_r_button = tk.Radiobutton(self, text = "Mean", variable = self.adaptive_method_var, value = 1)
        self.mean_method_r_button.grid(row = 3, column = 0)
        self.gaussian_method_r_button = tk.Radiobutton(self, text = "Gaussian", variable = self.adaptive_method_var, value = 2)
        self.gaussian_method_r_button.grid(row = 3, column = 1)
        self.adaptive_method_var.set(1)

    def get_window_size(self):
        return int(self.window_size_var.get())

    def get_c_constant(self):
        return int(self.c_constant_var.get())

    def get_adaptive_threshold_method(self):
        if self.adaptive_method_var.get() == 1:
            return "mean"
        else:
            return "gussian"

    def set_segmentation_image(self, rgb_image):
        self.segmentation_visualizer.image_canvas.set_image(rgb_image)

    def force_only_odd_values(self):        
        current_value = int(self.window_size_var.get())
        if current_value == self.previous_window_size_value:
            return
        if current_value % 2 == 0:
            if current_value < self.previous_window_size_value:
                current_value -= 1
            else:
                current_value += 1
        self.previous_window_size_value = current_value
        self.window_size_var.set(str(current_value))
        