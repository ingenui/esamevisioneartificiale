import cv2
import tkinter as tk
from customWidgets import ImageVisualizer

four_way_connectivity = 4
eight_way_connectivity = 8

class ConnectedComponentsFrame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text="Connected components", padx=5, pady=5)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=91)
        self.rowconfigure(1, weight=3)
        self.rowconfigure(2, weight=3)       
       
        self.grid_propagate(0)
        self.grid(sticky="nswe")
        self.createWidgets()        

    def createWidgets(self):
        self.connected_components_visualizer = ImageVisualizer.ImageVisualizer("CCL", self)
        self.connected_components_visualizer.grid(row=0, column=0, padx=3, pady=3, columnspan = 2)
        
        self.connectivity_var = tk.IntVar()
        self.four_way_connectivity_r_button = tk.Radiobutton(self, text = "4-way", variable = self.connectivity_var, value = four_way_connectivity)                
        self.four_way_connectivity_r_button.grid(row = 1, column = 0)
        self.eight_way_connectivity_r_button = tk.Radiobutton(self, text = "8-way", variable = self.connectivity_var, value = eight_way_connectivity)                
        self.eight_way_connectivity_r_button.grid(row = 1, column = 1)

        self.connectivity_var.set(eight_way_connectivity)

        
        self.percentage_threshold_var = tk.StringVar()
        self.percentage_threshold_spinbox = tk.Spinbox(self, from_ = 0.01, to = 0.9, 
                                                       state = 'readonly', 
                                                       textvariable = self.percentage_threshold_var,
                                                       increment = 0.01)
        self.previous_percentage_threshold_value = 0.1
        self.percentage_threshold_var.set(str(self.previous_percentage_threshold_value))
        self.percentage_threshold_spinbox.grid(row = 2, column = 0, columnspan = 2)

    def get_connectivity(self):
        return self.connectivity_var.get()

    def get_percentage_threshold(self):
        return float(self.percentage_threshold_var.get())

    def set_connected_components_image(self, image):
        self.connected_components_visualizer.set_image(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))