import tkinter as tk
import cv2
import numpy as np
from Homeworks.Homework4 import homework4 as hw
from customWidgets import ImageVisualizer
import threading

segmentation = 1
morphology = 2
connected_components_labeling = 3

class PipelineBlendFrame(tk.LabelFrame):
    """description of class"""

    def __init__(self, homework4, master=None):
        tk.LabelFrame.__init__(self, master, text="Blend", padx=5, pady=5)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=1)
        self.rowconfigure(0, weight=97)
        self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)
        self.rowconfigure(3, weight=1)

        self.grid_propagate(0)
        self.grid(sticky="nswe")
        self.hw4 = homework4
        self.createWidgets()        

    def createWidgets(self):
        self.blend_visualizer = ImageVisualizer.ImageVisualizer("Blend", self)
        self.blend_visualizer.grid(row = 0, column = 0, padx = 3, pady = 3, columnspan = 3)

        self.pipeline_step_var = tk.IntVar()
        self.segmentation_step_r_button = tk.Radiobutton(self, text = "Segmentation", variable = self.pipeline_step_var, value = segmentation)
        self.segmentation_step_r_button.config(state = tk.DISABLED)
        self.pipeline_step_var.set(segmentation)
        self.segmentation_step_r_button.grid(row = 1, column = 0, columnspan = 3)

        self.morphology_step_r_button = tk.Radiobutton(self, text = "Morphology", variable = self.pipeline_step_var, value = morphology)
        self.morphology_step_r_button.config(state = tk.DISABLED)        
        self.morphology_step_r_button.grid(row = 2, column = 0, columnspan = 3)

        self.ccl_r_button = tk.Radiobutton(self, text = "CCL", variable = self.pipeline_step_var, value = connected_components_labeling)
        self.ccl_r_button.config(state = tk.DISABLED)
        self.ccl_r_button.grid(row = 3, column = 0, columnspan = 3)

        self.original_image_scale_label = tk.Label(self, text = "Original")
        self.original_image_scale_label.config(state = tk.DISABLED)
        self.original_image_scale_label.grid(row = 4, column = 0)

        self.blend_scale = tk.Scale(self, from_ = 0, to_ = 100, orient = tk.HORIZONTAL, command = self.wait_before_blending)
        self.blend_scale.config(state = tk.DISABLED)
        self.blend_scale.grid(row = 4, column = 1)

        self.original_image_scale_label = tk.Label(self, text = "Selected")
        self.original_image_scale_label.config(state = tk.DISABLED)
        self.original_image_scale_label.grid(row = 4, column = 2)
       
        self.counting = False
        self.tenth_of_second_counter = 0

    def enable(self):
        self.blend_scale.config(state = tk.NORMAL)
        self.segmentation_step_r_button.config(state = tk.NORMAL)
        self.morphology_step_r_button.config(state = tk.NORMAL)
        self.ccl_r_button.config(state = tk.NORMAL)
        self.original_image_scale_label.config(state = tk.NORMAL)
        self.blend_scale.config(state = tk.NORMAL)

    def wait_before_blending(self, new_value):
        self.tenth_of_second_counter = 0
        if not self.counting:
            self.counting = True
            threading.Timer(0.1, self.increment_wait_counter).start()

    def increment_wait_counter(self):        
        self.tenth_of_second_counter += 1
        if self.tenth_of_second_counter == 5:
            self.counting = False
            self.blend()
        else:
            threading.Timer(0.1, self.increment_wait_counter).start()

    def blend(self):
        selected_step = self.pipeline_step_var.get()

        if selected_step == segmentation:
            selected_image = cv2.cvtColor(self.hw4.get_segmented_image(), cv2.COLOR_GRAY2BGR)
        elif selected_step == morphology:
            selected_image = cv2.cvtColor(self.hw4.get_morphology_image(), cv2.COLOR_GRAY2BGR)
        elif selected_step == connected_components_labeling:
            selected_image = self.hw4.get_connected_components_image()
        
        original_image = self.hw4.get_original_image()
        alpha = self.blend_scale.get() / 100.0
        blended_image = ((1 - alpha) * original_image + alpha * selected_image).astype(np.uint8)
        blended_image = cv2.cvtColor(blended_image, cv2.COLOR_BGR2RGB)
        self.blend_visualizer.set_image(blended_image)