import threading
import time

class ThreadedFrameUpdater(threading.Thread):

    def __init__(self, update_callback, frame_rate = 10.0):
        threading.Thread.__init__(self)

        self.can_update = True
        self.callback = update_callback
        self.sleep_time = 1.0 / float(frame_rate)

    def stop(self):
        self.can_update = False

    def run(self):
        while self.can_update:
            self.callback()
            #time.sleep(self.sleep_time)