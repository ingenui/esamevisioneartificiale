import cv2
from darkflow.net.build import TFNet
import numpy as np
import time
import sys
from PIL import Image

(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')

if __name__ == '__main__':
    option = {
        'model': 'cfg/yolov2.cfg',
        'load': 'C:/Users/picio/Documents/darkflow-master/bin/yolov2.weights',
        'threshold': 0.15,
        'gpu': 1.0
    }

    tfnet = TFNet(option)

    capture = cv2.VideoCapture('Camera5.mp4')
    colors = [tuple(255 * np.random.rand(3)) for i in range(5)]

    while (capture.isOpened()):
        stime = time.time()
        ret, frame = capture.read()
        if ret:
            results = tfnet.return_predict(frame)
            for color, result in zip(colors, results):
                tl = (result['topleft']['x'], result['topleft']['y'])
                br = (result['bottomright']['x'], result['bottomright']['y'])
                label = result['label']
                frame = cv2.rectangle(frame, tl, br, color, 7)
                frame = cv2.putText(frame, label, tl, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2)
            cv2.imshow('frame', frame)
            print('FPS {:.1f}'.format(1 / (time.time() - stime)))
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            capture.release()
            cv2.destroyAllWindows()
            break


    # video = cv2.VideoCapture("C:\Users\picio\Videos\Camera6.mp4")
    # i = 0
    # videoname = 'Camera6_'
    # while True:
    #     ok, frame = video.read()
    #     name = videoname+str(i)+'.jpeg'
    #     im = Image.fromarray(frame)
    #     im.save(name)
    #     if not ok:
    #         break
    #     i+=1

    # tracker_types = ['BOOSTING', 'MIL', 'KCF', 'TLD', 'MEDIANFLOW', 'GOTURN']
    # tracker_type = tracker_types[1]
    #
    # if int(minor_ver) < 3:
    #     tracker = cv2.Tracker_create(tracker_type)
    # else:
    #     if tracker_type == 'BOOSTING':
    #         tracker = cv2.TrackerBoosting_create()
    #     if tracker_type == 'MIL':
    #         tracker = cv2.TrackerMIL_create()
    #     if tracker_type == 'KCF':
    #         tracker = cv2.TrackerKCF_create()
    #     if tracker_type == 'TLD':
    #         tracker = cv2.TrackerTLD_create()
    #     if tracker_type == 'MEDIANFLOW':
    #         tracker = cv2.TrackerMedianFlow_create()
    #     if tracker_type == 'GOTURN':
    #         tracker = cv2.TrackerGOTURN_create()
    #
    # # Read video
    # video = cv2.VideoCapture("C:\Users\picio\Videos\Camera6.mp4")
    # video.set(cv2.CAP_PROP_POS_MSEC, 162000)
    #
    # # Exit if video not opened.
    # if not video.isOpened():
    #     print "Could not open video"
    #     sys.exit()
    #
    # # Read first frame.
    # ok, frame = video.read()
    # frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    # if not ok:
    #     print 'Cannot read video file'
    #     sys.exit()
    #
    # # Define an initial bounding box
    # bbox = (287, 23, 86, 320)
    #
    # # Uncomment the line below to select a different bounding box
    # bbox = cv2.selectROI(frame, False)
    #
    # # Initialize tracker with first frame and bounding box
    # cacca, temp = video.read()
    # temp = cv2.cvtColor(temp, cv2.COLOR_RGB2GRAY)
    # ok = tracker.init(abs( temp.astype(np.int16) - frame.astype(np.int16)).astype(np.uint8), bbox)
    #
    # while True:
    #     frame_prec = frame.copy()
    #     # Read a new frame
    #     ok, frame = video.read()
    #     frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    #     frame_diff = abs((frame.astype(np.int16) - frame_prec.astype(np.int16))).astype(np.uint8)
    #     frame_diff[frame_diff<20]=0
    #     frame_diff*=10
    #
    #     if not ok:
    #         break
    #
    #     # Start timer
    #     timer = cv2.getTickCount()
    #
    #     # Update tracker
    #     ok, bbox = tracker.update(frame_diff)
    #
    #     # Calculate Frames per second (FPS)
    #     fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer);
    #
    #     # Draw bounding box
    #     if ok:
    #         # Tracking success
    #         p1 = (int(bbox[0]), int(bbox[1]))
    #         p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
    #         cv2.rectangle(frame, p1, p2, (255, 0, 0), 2, 1)
    #     else:
    #         # Tracking failure
    #         cv2.putText(frame, "Tracking failure detected", (100, 80), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2)
    #
    #     # Display tracker type on frame
    #     cv2.putText(frame, tracker_type + " Tracker", (100, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50, 170, 50), 2);
    #
    #     # Display FPS on frame
    #     cv2.putText(frame, "FPS : " + str(int(fps)), (100, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50, 170, 50), 2);
    #
    #     # Display result
    #     cv2.imshow("Tracking", frame)
    #     cv2.imshow("differenze", frame_diff)
    #
    #
    #     # Exit if ESC pressed
    #     k = cv2.waitKey(1) & 0xff
    #     if k == 27: break