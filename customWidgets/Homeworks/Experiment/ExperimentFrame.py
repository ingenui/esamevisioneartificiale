import tkinter as tk
import threading
from customWidgets import ImageVisualizer
from Homeworks.Experiment import experiment
from customWidgets.Homeworks.Experiment.ThrededFrameUpdater import ThreadedFrameUpdater
import time

class ExperimentFrame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Homework 2", padx = 5, pady = 5)
        self.columnconfigure(0, weight = 1)
        self.columnconfigure(1, weight = 1)
        self.rowconfigure(0, weight = 2)
        self.rowconfigure(1, weight = 98)

        self.grid_propagate(0)
        self.grid(sticky = "nswe")
        self.createWidgets()
        self.exp = experiment.experiment()        
        self.video_started = False

    def createWidgets(self):
        self.fps_label = tk.Label(self, text = "fps")
        self.fps_label.grid(row = 0, column = 1)
    
        self.start_stop_button = tk.Button(self, text = "Start", command = self.start_stop_video)
        self.start_stop_button.grid(row = 0, column = 0, sticky = "n")
        
        self.video_frame_visualizer = ImageVisualizer.ImageVisualizer("Video", self)
        self.video_frame_visualizer.grid(row = 1, column = 0, padx = 3, pady = 3, columnspan = 2)

    def start_stop_video(self):
        if self.video_started:
            self.exp.stop_video_capture()
            self.start_stop_button.config(text = "Start")
            self.updater.stop()
        else:
            self.updater = ThreadedFrameUpdater(self.update_video)
            self.exp.start_video_capture()
            self.start_stop_button.config(text = "Stop")            
            self.updater.start()
        
        self.video_started = not self.video_started

    def update_video(self):        
        next_frame = self.exp.get_next_frame()
        if not next_frame is None:
            start_time = time.time()
            self.video_frame_visualizer.set_image(next_frame)
            self.fps_label.config(text = "{} fps".format(int((1. / (time.time() - start_time)))))