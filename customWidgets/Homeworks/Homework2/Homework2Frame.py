import tkinter as tk
from tkinter import filedialog
from Homeworks.Homework2 import homework2 as hw
from customWidgets import ImageVisualizer
from customWidgets.Homeworks.Homework2 import NoiseSliders

class Homework2Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Homework 2", padx = 5, pady = 5)
        self.columnconfigure(0, weight = 1)       
        self.rowconfigure(0, weight = 1)
        self.rowconfigure(1, weight = 48)
        self.rowconfigure(2, weight = 48)
        self.rowconfigure(3, weight = 3)

        self.grid_propagate(0)
        self.grid(sticky = "nswe")
        self.createWidgets()
        self.hw2 = hw.homework2()

    def createWidgets(self):
        self.chooseFileButton = tk.Button(self, text = "Choose file", command = self.choose_iconic_image_path)
        self.chooseFileButton.grid(row = 0, column = 0, sticky = "n")

        self.iconic_image_visualizer = ImageVisualizer.ImageVisualizer("Original", self)
        self.iconic_image_visualizer.grid(row = 1, column = 0, padx = 3, pady = 3)

        self.noised_image_visualizer = ImageVisualizer.ImageVisualizer("Normal noise", self)
        self.noised_image_visualizer.grid(row = 2, column = 0, padx = 3, pady = 3)
        
        self.noise_sliders = NoiseSliders.NoiseSliders(self.noise_mean_changed, self.noise_std_dev_changed, self)
        self.noise_sliders.set_mean(0)
        self.noise_sliders.set_std_dev(1)
        self.noise_sliders.disable()
        self.noise_sliders.grid(row = 3, column = 0, sticky = "n")

    def choose_iconic_image_path(self):
        self.hw2 = hw.homework2()
        self.iconic_image_path = filedialog.askopenfilename()
        self.hw2.load_iconic_image(self.iconic_image_path)
        self.iconic_image = self.hw2.get_rgb_iconic_image()
        self.iconic_image_visualizer.set_image(self.iconic_image)
                
        self.update_noise_image()
        self.noise_sliders.enable()
        
    def noise_mean_changed(self, new_mean):
        state = self.noise_sliders.noise_mean_scale.cget("state")
        if (state == "active" or state == "normal"):
            self.update_noise_image()

    def noise_std_dev_changed(self, new_variance):
        state = self.noise_sliders.noise_std_dev_scale.cget("state")
        if (state == "active" or state == "normal"):
            self.update_noise_image()

    def update_noise_image(self):
        current_mean = self.noise_sliders.get_mean()
        current_std_dev = self.noise_sliders.get_std_dev()
        self.noised_image = self.hw2.get_rgb_noised_image(current_mean, current_std_dev)
        self.noised_image_visualizer.set_image(self.noised_image)