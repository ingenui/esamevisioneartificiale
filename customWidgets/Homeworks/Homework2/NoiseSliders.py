import tkinter as tk

class NoiseSliders(tk.Frame):
    """description of class"""

    def __init__(self, mean_changed_command, std_dev_changed_command, master=None):
        if mean_changed_command is None:
            raise ValueError("mean_changed_command must not be None")
        if std_dev_changed_command is None:
            raise ValueError("variance_changed_command must not be None")

        self.mean_changed_command = mean_changed_command
        self.std_dev_changed_command = std_dev_changed_command

        tk.Frame.__init__(self, master)
        self.grid()
        self.create_widgets()

    def create_widgets(self):        
        self.noise_mean_scale = tk.Scale(self, from_ = -30, to_ = 30, command = self.mean_changed_command, orient = tk.HORIZONTAL, label = "Mean")
        self.noise_mean_scale.grid(row = 0, column = 0, sticky = "n")
        self.noise_std_dev_scale = tk.Scale(self, from_ = 0, to_ = 20, command = self.std_dev_changed_command, orient = tk.HORIZONTAL, label = "Std. Deviation")
        self.noise_std_dev_scale.grid(row = 0, column = 1, sticky = "n")

    def disable(self):
        self.noise_mean_scale.config(state = tk.DISABLED)
        self.noise_std_dev_scale.config(state = tk.DISABLED)

    def enable(self):
        self.noise_mean_scale.config(state = tk.NORMAL)
        self.noise_std_dev_scale.config(state = tk.NORMAL)

    def set_mean(self, value):
        self.noise_mean_scale.set(value)

    def set_std_dev(self, value):
        self.noise_std_dev_scale.set(value)

    def get_mean(self):
        return self.noise_mean_scale.get()

    def get_std_dev(self):
        return self.noise_std_dev_scale.get()