import tkinter as tk
import PIL

from customWidgets import ResizingCanvas

class ImageVisualizer(tk.LabelFrame):
    """description of class"""

    def __init__(self, image_name, master=None):
        tk.LabelFrame.__init__(self, master, text = image_name)

        self.columnconfigure(0, weight = 1)            
        self.rowconfigure(0, weight = 1)

        self.image_name = image_name

        self.grid(sticky = "nswe")
        self.grid_propagate(0)
        self.createWidgets()        

    def createWidgets(self):
        self.image_canvas = ResizingCanvas.ResizingCanvas(self, bd = 1, relief = tk.RAISED) #RAISED per debug
        self.image_canvas.bind("<Double-Button-1>", self.show_full_image)
        self.image_canvas.grid(row = 0, column = 0, sticky = "nswe")

    def show_full_image(self, event):
        if hasattr(self.image_canvas, "image") and not self.image_canvas.image is None:
            PIL.Image.fromarray(self.image_canvas.image).show(self.image_name)

    def set_image(self, rgb_image):
        """ Sets the rgb image to be displayed on the frame"""
        self.image_canvas.set_image(rgb_image)