import tkinter as tk
from tkinter import ttk
from PIL import Image

class RankingVisualizer(tk.LabelFrame):
    """description of class"""

    def __init__(self, title="Ranking", master=None):
        tk.LabelFrame.__init__(self, master, text = title)
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

        self.ranking_listbox = tk.Listbox(self, selectmode=tk.SINGLE)
        self.ranking_listbox.bind("<Double-Button-1>", self.__click)
        self.grid()
        self.ranking_listbox.grid(row=0, column=0, sticky="nsew")
        self.ranking_scrollbar = ttk.Scrollbar(self)
        self.ranking_scrollbar.grid(row=0, column=1, sticky="NS")
        self.ranking_listbox.configure(yscrollcommand=self.ranking_scrollbar.set)
        self.images=[]

    def add_ranking(self, ranking_list):
        self.ranking_listbox.delete(0, tk.END)
        for element in ranking_list:
            self.images.append((element[0], element[1], element[2]))
            self.ranking_listbox.insert("end", element[1] + ", {0:.3f}".format(element[0]))

    def __click(self, event):
        selection_indices = self.ranking_listbox.curselection()
        Image.open(self.images[selection_indices[0]][2]).show()

        print("Prova clic")