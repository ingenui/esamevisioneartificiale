import threading

class ThreadedLab11Runner(threading.Thread):
    
    def __init__(self, queue, lab11):
        threading.Thread.__init__(self)
        self.lab11 = lab11
        self.queue = queue
        self.keep_running = True

    def stop_running(self):
        self.keep_running = False

    def run(self):
        while self.keep_running:
            rgb = self.lab11.get_current_optical_flow()
            self.queue.put(rgb)

        self.lab11.release_camera()
