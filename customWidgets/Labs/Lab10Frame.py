import tkinter as tk
from tkinter import filedialog
from customWidgets import ImageVisualizer
from customWidgets.Labs import ContrastSliders
from Labs.Lab10 import lab10
from tkinter import messagebox
import cv2

class Lab10Frame(tk.LabelFrame):
    def __init__(self, master=None):
        self.frame_number = 0
        tk.LabelFrame.__init__(self, master, text="Laboratorio 10", padx=5, pady=5)
        self.columnconfigure(0, weight=3)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=5)

        self.grid_propagate(0)
        self.grid(sticky="nswe")
        self.createWidgets()
        self.lab10 = lab10.Lab10()

    def createWidgets(self):
        self.chooseVideoButton = tk.Button(self, text="Choose file", command=self.choose_video_path)
        self.chooseVideoButton.grid(row=0, column=0, sticky="n")
        self.nextFrameButton = tk.Button(self, text="Next frame", command=self.next_frame)
        self.nextFrameButton.grid(row=0, column=1, sticky="n")
        self.frameVisualizer = ImageVisualizer.ImageVisualizer("Video", self)
        self.frameVisualizer.image_canvas.bind('<Return>', lambda x: self.next_frame())
        self.frameVisualizer.grid(row=1, column=0, padx=3, pady=3)
        self.frameVisualizer.image_canvas.focus_set()
        self.boundingBoxesCoord = tk.Text(self, width=20)
        self.boundingBoxesCoord.grid(row=1, column=1, padx=3, pady=3, sticky='n')


    def choose_video_path(self):
        self.video_path = filedialog.askopenfilename()
        try:
            frame = self.lab10.set_video(self.video_path)
            if not self.lab10.video.isOpened():
                print("Could not open video")
                messagebox.showinfo("Error", "Could not open this video")
                return
        except Exception as e:
            messagebox.showinfo("Error", str(e))
            self.video_path = None
            return
        self.frameVisualizer.image_canvas.delete_image()
        self.frameVisualizer.set_image(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        if len(self.lab10.faces)==0:
            self.boundingBoxesCoord.config(state='normal')
            self.boundingBoxesCoord.delete('1.0', tk.END)
            self.boundingBoxesCoord.insert('1.0', 'No faces in the first frame')
            self.boundingBoxesCoord.config(state='disabled')
        else:
            self.boundingBoxesCoord.config(state='normal')
            self.boundingBoxesCoord.delete('1.0', tk.END)
            self.boundingBoxesCoord.insert('1.0', 'Frame 0\n')
            for idx, f in enumerate(self.lab10.faces):
                self.boundingBoxesCoord.tag_add('highlightline', str(idx * 5 + 1)+'.0', str(idx * 5 + 2)+'.0')
                self.boundingBoxesCoord.insert(str(idx * 5 + 1)+'.0', 'Face ' + str(idx)+'\n')
                self.boundingBoxesCoord.insert(str(idx * 5 + 2)+'.0', 'x :' + str(f[0])+'\n')
                self.boundingBoxesCoord.insert(str(idx * 5 + 3)+'.0', 'y :' + str(f[1])+'\n')
                self.boundingBoxesCoord.insert(str(idx * 5 + 4)+'.0', 'h :' + str(f[2])+'\n')
                self.boundingBoxesCoord.insert(str(idx * 5 + 5)+'.0', 'w :' + str(f[3])+'\n')
            self.boundingBoxesCoord.config(state='disabled')
        self.frame_number = 0
        self.frameVisualizer.image_canvas.focus_set()

    def next_frame(self, event=None):
        try:
            frame = self.lab10.nextFrame()
            self.frameVisualizer.set_image(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        except Exception as e:
            self.frameVisualizer.image_canvas.delete_image()
            self.frameVisualizer.image_canvas.set_error_message(str(e))
            self.boundingBoxesCoord.config(state='normal')
            self.boundingBoxesCoord.delete('1.0', tk.END)
            self.boundingBoxesCoord.config(state='disabled')
        self.frame_number += 1
        self.boundingBoxesCoord.config(state='normal')
        self.boundingBoxesCoord.delete('1.0', tk.END)
        self.boundingBoxesCoord.grid(row=1, column=1, padx=3, pady=3, sticky='n')
        self.boundingBoxesCoord.insert('1.0', 'Frame '+str(self.frame_number)+'\n')
        for idx, f in enumerate(self.lab10.faces):
            self.boundingBoxesCoord.insert(str(idx * 5 + 1) + '.0', 'Face ' + str(idx) + '\n')
            self.boundingBoxesCoord.insert(str(idx * 5 + 2) + '.0', 'x :' + str(f[0]) + '\n')
            self.boundingBoxesCoord.insert(str(idx * 5 + 3) + '.0', 'y :' + str(f[1]) + '\n')
            self.boundingBoxesCoord.insert(str(idx * 5 + 4) + '.0', 'h :' + str(f[2]) + '\n')
            self.boundingBoxesCoord.insert(str(idx * 5 + 5) + '.0', 'w :' + str(f[3]) + '\n')
            self.boundingBoxesCoord.tag_add('highlightline', str(idx * 5 + 1) + '.0', str(idx * 5 + 2) + '.0')
        self.boundingBoxesCoord.config(state='disabled')