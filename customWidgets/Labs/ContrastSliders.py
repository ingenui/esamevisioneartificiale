import tkinter as tk

class ContrastSliders(tk.Frame):
    """description of class"""
    contrast_range_min = 0
    contrast_range_max = 255

    def __init__(self, minmax_changed_command, master=None):
        if minmax_changed_command is None:
            raise ValueError("min_changed_command must not be None")

        self.minmax_changed_command = minmax_changed_command

        tk.Frame.__init__(self, master)
        self.grid()
        self.create_widgets()

    def create_widgets(self):        
        self.min_scale = tk.Scale(self, from_ = ContrastSliders.contrast_range_min, to_ = ContrastSliders.contrast_range_max, command = self.minmax_changed_command, orient = tk.HORIZONTAL, label ="Min")
        self.min_scale.grid(row = 0, column = 0, sticky ="n")
        self.max_scale = tk.Scale(self, from_ = ContrastSliders.contrast_range_min, to_ = ContrastSliders.contrast_range_max, command = self.minmax_changed_command, orient = tk.HORIZONTAL, label ="Max")
        self.max_scale.grid(row = 0, column = 1, sticky ="n")

    def disable(self):
        self.min_scale.config(state = tk.DISABLED)
        self.max_scale.config(state = tk.DISABLED)

    def enable(self):
        self.min_scale.config(state = tk.NORMAL)
        self.max_scale.config(state = tk.NORMAL)

    def set_min(self, value):
        if value < ContrastSliders.contrast_range_min:
            raise Exception("Value too small")
        if value > self.get_max():
            raise Exception("Value greater than max")
        self.min_scale.set(value)

    def set_max(self, value):
        if value > ContrastSliders.contrast_range_max:
            raise Exception("Value too big")
        if value < self.get_min():
            raise Exception("Value smaller than small")
        self.max_scale.set(value)

    def get_min(self):
        return self.min_scale.get()

    def get_max(self):
        return self.max_scale.get()