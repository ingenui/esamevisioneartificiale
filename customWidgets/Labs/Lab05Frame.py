import tkinter as tk
from tkinter import filedialog, ttk, messagebox
from tkinter.filedialog import askdirectory
import queue
from customWidgets import ImageVisualizer, CliccableImageVisualizer

from Labs.Lab05 import lab05

class Lab05Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Laboratorio 5", padx = 5, pady = 5)
        self.columnconfigure(0, weight = 20)
        self.columnconfigure(1, weight = 50)
        self.columnconfigure(2, weight = 30)
        self.rowconfigure(0, weight = 3)
        self.rowconfigure(1, weight = 49)
        self.rowconfigure(2, weight = 48)

        self.grid_propagate(0)
        self.grid(sticky = "nswe")
        self.lab5 = lab05.Lab5()
        self.createWidgets()

        self.canvas_vertices = []
        self.canvas_circles = []
        self.canvas_lines = []

        self.stitched_images = []

        self.bind_all("<Control-z>", self.delete_last_vertex)

    def createWidgets(self):
        self.choose_file_button = tk.Button(self, text="Choose file", command=self.choose_sticky_image_path)
        self.choose_file_button.grid(row=0, column=0, sticky="n")

        self.sticky_image_visualizer = ImageVisualizer.ImageVisualizer("Sticky", self)
        self.sticky_image_visualizer.grid(row=1, column=0, padx=3, pady=3)

        self.choose_background_button = tk.Button(self, text="Choose file", command=self.choose_background_image_path)
        self.choose_background_button.grid(row=0, column=1, sticky="n")

        self.background_image_visualizer = CliccableImageVisualizer.CliccableImageVisualizer("Background", self.point_click, self)
        self.background_image_visualizer.grid(row=1, column=1, padx=3, pady=3, columnspan=3, rowspan=2)

    def point_click(self, event):
        if not self.background_image_visualizer.image_canvas.has_image() or not self.sticky_image_visualizer.image_canvas.has_image():
            return

        canvas_coordinates = (event.x, event.y)
        open_polygon_vertices = self.lab5.get_open_polygon_vertices()
        draw_quadrilateral_sides = len(open_polygon_vertices) > 0        
        close_quadrilateral = len(open_polygon_vertices) == 3
        
        dx, dy = self.background_image_visualizer.image_canvas.get_image_position()
        image_coordinates = (int((event.x - dx) / self.background_image_visualizer.image_canvas.get_current_ratio()), 
                                int((event.y - dy) / self.background_image_visualizer.image_canvas.get_current_ratio()))
        is_convex = self.lab5.add_vertex(image_coordinates)
                        
        radius = 3
        cx, cy = self.get_canvas_coordinates_from_image_coordinates(image_coordinates)
        new_circle = self.background_image_visualizer.image_canvas.create_oval(cx - radius,
                                                                               cy - radius, 
                                                                               cx + radius,
                                                                               cy + radius, 
                                                                               fill="red")
        self.canvas_circles.append(new_circle)
        if (is_convex):
            if close_quadrilateral:
                for c in self.canvas_circles:
                    self.background_image_visualizer.image_canvas.delete(c)
                for l in self.canvas_lines:
                    self.background_image_visualizer.image_canvas.delete(l)
                self.canvas_circles.clear()
                self.canvas_lines.clear()
                new_stiched_background = self.lab5.warp_sticky_on_background_last_polygon(self.stitched_images[0])
                self.background_image_visualizer.set_image(new_stiched_background)
                self.stitched_images.insert(0, new_stiched_background.copy())
            elif draw_quadrilateral_sides:
                open_polygon_vertices = self.lab5.get_open_polygon_vertices()
                x0, y0 = self.get_canvas_coordinates_from_image_coordinates((open_polygon_vertices[-2].x, open_polygon_vertices[-2].y))
                x1, y1 = self.get_canvas_coordinates_from_image_coordinates((open_polygon_vertices[-1].x, open_polygon_vertices[-1].y))
                new_line = self.background_image_visualizer.image_canvas.create_line(x0, y0, x1, y1)
                self.canvas_lines.append(new_line)            
        else:
            self.background_image_visualizer.image_canvas.delete(new_circle)
            messagebox.showwarning("Warning", "Draw a convex polygon")

    def get_canvas_coordinates_from_image_coordinates(self, image_coords):
        dx, dy = self.background_image_visualizer.image_canvas.get_image_position()
        x = int(image_coords[0] * self.background_image_visualizer.image_canvas.get_current_ratio() + dx)
        y = int(image_coords[1] * self.background_image_visualizer.image_canvas.get_current_ratio() + dy)
        return x, y

    def delete_last_vertex(self, event):
        vertices = self.lab5.get_open_polygon_vertices()
        if len(vertices)==0 and len(self.lab5.polygons) == 0:
            return
        self.lab5.delete_last_vertex()
        vertices = self.lab5.get_open_polygon_vertices()
        is_last_vertex_of_quadrilateral = len(vertices) == 3
        is_first_vertex_of_quadrilateral = len(vertices) == 0
        if is_last_vertex_of_quadrilateral:
            self.stitched_images.pop(0)
            self.background_image_visualizer.set_image(self.stitched_images[0])
            dx, dy = self.background_image_visualizer.image_canvas.get_image_position()
            radius = 3
            first_vertex=True
            x_prev = -1
            y_prev = -1
            for point in vertices:
                x = point.coords.xy[0][0]
                y = point.coords.xy[1][0]
                x = int(x * self.background_image_visualizer.image_canvas.get_current_ratio() + dx)
                y = int(y * self.background_image_visualizer.image_canvas.get_current_ratio() + dy)
                new_circle = self.background_image_visualizer.image_canvas.create_oval(x - radius,
                                                                                       y - radius,
                                                                                       x + radius,
                                                                                       y + radius,
                                                                                       fill="red")
                self.canvas_circles.append(new_circle)
                if not first_vertex:
                    new_line = self.background_image_visualizer.image_canvas.create_line(x, y, x_prev, y_prev)
                    self.canvas_lines.append(new_line)
                first_vertex = False
                x_prev = x
                y_prev = y
        elif is_first_vertex_of_quadrilateral:
            self.background_image_visualizer.image_canvas.delete(self.canvas_circles[-1])
            del self.canvas_circles[-1]
        else:
            self.background_image_visualizer.image_canvas.delete(self.canvas_lines[-1])
            del self.canvas_lines[-1]
            self.background_image_visualizer.image_canvas.delete(self.canvas_circles[-1])
            del self.canvas_circles[-1]

    def choose_sticky_image_path(self):
        self.sticky_image_path = filedialog.askopenfilename()
        if self.sticky_image_path == "":
            return
        self.lab5.set_sticky_image(self.sticky_image_path)
        self.sticky_image_visualizer.set_image(self.lab5.get_sticky_image())

    def choose_background_image_path(self):
        self.background_image_path = filedialog.askopenfilename()
        if self.background_image_path == "":
            return
        self.lab5.set_background_image(self.background_image_path)
        self.background_image_visualizer.set_image(self.lab5.get_background_image())
        self.stitched_images.insert(0,self.lab5.get_background_image().copy())