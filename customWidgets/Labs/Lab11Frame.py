import tkinter as tk
import Labs.Lab11.lab11 as lab11
import customWidgets.Labs.ThreadedLab11Runner
import queue
from customWidgets import ImageVisualizer

class Lab11Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Laboratorio 11", padx = 5, pady = 5)
        self.columnconfigure(0, weight = 1)        
        self.rowconfigure(0, weight = 1)
        self.rowconfigure(1, weight = 99)
        
        self.grid_propagate(0)
        self.grid(sticky = "nswe")        
        self.createWidgets()

        self.running = False

        self.lab11_queue = queue.Queue()        

    def createWidgets(self):
        self.btn_text = tk.StringVar()
        self.btn_text.set("Start")

        self.start_stop_button = tk.Button(self, textvariable = self.btn_text, command = self.start_stop_button_clicked)
        self.start_stop_button.grid(row = 0, column = 0, sticky = "n")

        self.optical_flow_image_visualizer = ImageVisualizer.ImageVisualizer("Optical flow", self)
        self.optical_flow_image_visualizer.grid(row=1, column=0, padx=3, pady=3)

    def get_next_frame(self):
        try:
            rgb = self.lab11_queue.get()
            self.optical_flow_image_visualizer.set_image(rgb)        
        except:
            pass
            
        if self.running:
            self.master.after(100, self.get_next_frame)        

    def start_stop_button_clicked(self):        
        self.running = not self.running

        if self.running:            
            self.btn_text.set("Stop")
            self.lab11 = lab11.lab11()
            self.lab11_runner = customWidgets.Labs.ThreadedLab11Runner.ThreadedLab11Runner(self.lab11_queue, self.lab11)
            self.lab11_runner.start()
            self.master.after(100, self.get_next_frame)
        else:
            self.btn_text.set("Start")
            self.lab11_runner.stop_running()

        