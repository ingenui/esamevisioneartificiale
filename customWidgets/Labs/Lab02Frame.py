import tkinter as tk
from tkinter import filedialog
from Homeworks.Homework2 import homework2 as hw
from customWidgets import ImageVisualizer
from customWidgets.Labs import ContrastSliders
from Labs.Lab02 import lab02

class Lab02Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Laboratorio 2", padx = 5, pady = 5)
        self.columnconfigure(0, weight = 5)
        self.columnconfigure(1, weight = 1)
        self.columnconfigure(2, weight = 1)
        self.columnconfigure(3, weight = 1)
        self.columnconfigure(4, weight = 1)
        self.rowconfigure(0, weight = 1)
        self.rowconfigure(1, weight = 30)
        self.rowconfigure(2, weight = 1)
        self.rowconfigure(3, weight = 40)
        self.rowconfigure(4, weight = 1)
        self.rowconfigure(5, weight = 27)

        self.grid_propagate(0)
        self.grid(sticky = "nswe")
        self.createWidgets()
        self.lab2 = lab02.Lab2()

    def createWidgets(self):
        self.chooseFileButton = tk.Button(self, text = "Choose file", command = self.choose_iconic_image_path)
        self.chooseFileButton.grid(row = 0, column = 0, sticky = "n")

        self.iconic_image_visualizer = ImageVisualizer.ImageVisualizer("Original", self)
        self.iconic_image_visualizer.grid(row = 1, column = 0, padx = 3, pady = 3)

        self.contrast_stretching_image_visualizer = ImageVisualizer.ImageVisualizer("Contrast stretching", self)
        self.contrast_stretching_image_visualizer.grid(row = 3, column = 0, padx = 3, pady = 3)

        labelText = tk.StringVar()
        labelText.set("Avg filter dimension")
        labelDir = tk.Label(self, textvariable=labelText, height=4)
        labelDir.grid(row=0, column=1, sticky="nw")
        self.avg_filter_dimension_var = tk.StringVar()
        self.avg_filter_dimension_spinbox = tk.Spinbox(self, from_=1, to=99, state='readonly',
                                                       textvariable=self.avg_filter_dimension_var)
        self.previous_avg_filter_size_value = 3
        self.avg_filter_dimension_var.set(str(self.previous_avg_filter_size_value))
        self.avg_filter_dimension_spinbox.grid(row=0, column=1, sticky="nw")

        self.avg_filter_button = tk.Button(self, text = "Avg Convolution", command = self.avg_filter_convolution)
        self.avg_filter_button.grid(row = 0, column = 2, sticky = "n")

        self.avg_filter_image_visualizer = ImageVisualizer.ImageVisualizer("Average filter", self)
        self.avg_filter_image_visualizer.grid(row=1, column=1, columnspan=4, padx=3, pady=3)

        GausLabelText = tk.StringVar()
        GausLabelText.set("Gaussian filter dimension")
        GauslabelDir = tk.Label(self, textvariable=GausLabelText, height=4)
        GauslabelDir.grid(row=2, column=1, sticky="nw")
        self.gaussian_filter_dimension_var = tk.StringVar()
        self.gaussian_filter_dimension_spinbox = tk.Spinbox(self, from_=1, to=99, state='readonly',
                                                       textvariable=self.gaussian_filter_dimension_var)
        self.previous_gaussian_filter_size_value = 3
        self.gaussian_filter_dimension_var.set(str(self.previous_gaussian_filter_size_value))
        self.gaussian_filter_dimension_spinbox.grid(row=2, column=1, sticky="nw")

        self.gaussian_filter_button = tk.Button(self, text="Gaussian Convolution", command=self.gaussian_filter_convolution)
        self.gaussian_filter_button.grid(row=2, column=2, sticky="n")

        self.gaussian_filter_image_visualizer = ImageVisualizer.ImageVisualizer("Gaussian filter", self)
        self.gaussian_filter_image_visualizer.grid(row=3, column=1, columnspan=4, padx=3, pady=3)

        self.contrast_sliders = ContrastSliders.ContrastSliders(self.minmax_changed, self)
        self.contrast_sliders.set_max(1)
        self.contrast_sliders.set_min(1)
        self.contrast_sliders.disable()
        self.contrast_sliders.grid(row = 2, column = 0, sticky ="n")

        MedianLabelText = tk.StringVar()
        MedianLabelText.set("Median filter dimension")
        MedianlabelDir = tk.Label(self, textvariable=MedianLabelText, height=4)
        MedianlabelDir.grid(row=4, column=0, sticky="nw")
        self.median_filter_dimension_var = tk.StringVar()
        self.median_filter_dimension_spinbox = tk.Spinbox(self, from_=1, to=99, state='readonly',
                                                            textvariable=self.median_filter_dimension_var)
        self.previous_median_filter_size_value = 3
        self.median_filter_dimension_var.set(str(self.previous_median_filter_size_value))
        self.median_filter_dimension_spinbox.grid(row=4, column=0, sticky="nw")

        self.median_filter_button = tk.Button(self, text="Median Convolution",
                                                command=self.median_filter_convolution)
        self.median_filter_button.grid(row=4, column=0, sticky="ne")

        self.median_filter_image_visualizer = ImageVisualizer.ImageVisualizer("Median filter", self)
        self.median_filter_image_visualizer.grid(row=5, column=0, padx=3, pady=3)

        BilateralLabelText = tk.StringVar()
        BilaterallabelDir = tk.Label(self, textvariable=BilateralLabelText, height=4)
        BilaterallabelDir.grid(row=4, column=1, sticky="nw")

        SigmaColorLabelText = tk.StringVar()
        SigmaColorLabelDir = tk.Label(self, textvariable=SigmaColorLabelText, height=4)
        SigmaColorLabelDir.grid(row=4, column=2, sticky="nw")
        SigmaSpaceLabelText = tk.StringVar()
        SigmaSpaceLabelDir = tk.Label(self, textvariable=SigmaSpaceLabelText, height=4)
        SigmaSpaceLabelDir.grid(row=4, column=3, sticky="nw")

        BilateralLabelText.set("Bilateral filter dimension")
        SigmaColorLabelText.set("Sigma Color")
        SigmaSpaceLabelText.set("Sigma Space")

        self.previous_bilateral_filter_size_value = 3
        self.previous_sigmaColor_filter_size_value = 200
        self.previous_sigmaSpace_filter_size_value = 200

        self.bilateral_filter_button = tk.Button(self, text="Bilateral Convolution", command=self.bilateral_filter_convolution)

        self.bilateral_filter_dimension_var = tk.StringVar()
        self.bilateral_filter_dimension_spinbox = tk.Spinbox(self, from_=1, to=99, state='readonly', textvariable=self.bilateral_filter_dimension_var)
        self.bilateral_filter_dimension_var.set(str(self.previous_bilateral_filter_size_value))

        self.sigmaColor_filter_dimension_var = tk.StringVar()
        self.sigmaColor_filter_dimension_spinbox = tk.Spinbox(self, from_=1, to=1000, state='readonly', textvariable=self.sigmaColor_filter_dimension_var)
        self.sigmaColor_filter_dimension_var.set(str(self.previous_sigmaColor_filter_size_value))

        self.sigmaSpace_filter_dimension_var = tk.StringVar()
        self.sigmaSpace_filter_dimension_spinbox = tk.Spinbox(self, from_=1, to=1000, state='readonly', textvariable=self.sigmaSpace_filter_dimension_var)
        self.sigmaSpace_filter_dimension_var.set(str(self.previous_sigmaSpace_filter_size_value))

        self.bilateral_filter_image_visualizer = ImageVisualizer.ImageVisualizer("Bilateral filter", self)

        self.bilateral_filter_button.grid(row=4, column=4, sticky="ne")
        self.bilateral_filter_dimension_spinbox.grid(row=4, column=1, sticky="nw")
        self.sigmaColor_filter_dimension_spinbox.grid(row=4, column=2, sticky="nw")
        self.sigmaSpace_filter_dimension_spinbox.grid(row=4, column=3, sticky="nw")
        self.bilateral_filter_image_visualizer.grid(row=5, column=1, padx=3, pady=3, columnspan=4)

    def choose_iconic_image_path(self):
        self.iconic_image_path = filedialog.askopenfilename()
        self.lab2.set_iconic_image(self.iconic_image_path)
        self.iconic_image = self.lab2.get_iconic_image()
        self.iconic_image_visualizer.set_image(self.iconic_image)
        self.contrast_sliders.enable()
        min, max = self.lab2.color_range()
        self.contrast_sliders.set_max(max)
        self.contrast_sliders.set_min(min)

    def minmax_changed(self, new_value):
        state = self.contrast_sliders.min_scale.cget("state")
        if (state == "active" or state == "normal"):
            self.update_contrast_image()

    def update_contrast_image(self):
        current_min = self.contrast_sliders.get_min()
        current_max = self.contrast_sliders.get_max()
        try:
            self.contrast_stretching_image_visualizer.set_image(self.lab2.contrast_stretching(self.lab2.get_iconic_image(), current_min, current_max))
        except Exception as e:
            self.contrast_stretching_image_visualizer.image_canvas.set_error_message(str(e))

    def avg_filter_convolution(self):
        try:
            self.avg_filter_image_visualizer.set_image(self.lab2.avg_filter_convolution(int(self.avg_filter_dimension_var.get())))
        except Exception as e:
            self.avg_filter_image_visualizer.image_canvas.set_error_message(str(e))

    def gaussian_filter_convolution(self):
        try:
            self.gaussian_filter_image_visualizer.set_image(self.lab2.gaussian_filter_convolution(int(self.gaussian_filter_dimension_var.get())))
        except Exception as e:
            self.gaussian_filter_image_visualizer.image_canvas.set_error_message(str(e))

    def median_filter_convolution(self):
        try:
            self.median_filter_image_visualizer.set_image(self.lab2.median_filter_convolution(int(self.median_filter_dimension_var.get())))
        except Exception as e:
            self.median_filter_image_visualizer.image_canvas.set_error_message(str(e))

    def bilateral_filter_convolution(self):
        try:
            self.bilateral_filter_image_visualizer.set_image(self.lab2.bilateral_filter_convolution(
                int(self.bilateral_filter_dimension_var.get()),
                int(self.sigmaColor_filter_dimension_var.get()),
                int(self.sigmaSpace_filter_dimension_var.get())))
        except Exception as e:
            self.bilateral_filter_image_visualizer.image_canvas.set_error_message(str(e))
