import tkinter as tk
from tkinter import filedialog
from Homeworks.Homework2 import homework2 as hw
from customWidgets import ImageVisualizer
from customWidgets.Labs import ContrastSliders
from Labs.Lab03 import lab03

class Lab03Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Laboratorio 3", padx = 5, pady = 5)
        self.columnconfigure(0, weight = 1)
        self.columnconfigure(1, weight = 1)
        self.rowconfigure(0, weight = 1)
        self.rowconfigure(1, weight = 5)
        self.rowconfigure(2, weight = 1)
        self.rowconfigure(3, weight = 5)
        self.rowconfigure(4, weight = 1)
        self.rowconfigure(5, weight = 5)


        self.grid_propagate(0)
        self.grid(sticky = "nswe")
        self.createWidgets()
        self.lab3 = lab03.Lab3()

    def createWidgets(self):
        self.chooseFileButton = tk.Button(self, text = "Choose file", command = self.choose_iconic_image_path)
        self.chooseFileButton.grid(row = 0, column = 0, sticky = "n")

        self.iconic_image_visualizer = ImageVisualizer.ImageVisualizer("Original", self)
        self.iconic_image_visualizer.grid(row = 1, column = 0, padx = 3, pady = 3)

        self.sobel_3_button = tk.Button(self, text ="Sobel 3", command = self.sobel_3)
        self.sobel_3_button.grid(row = 0, column = 1, sticky = "n")
        self.sobel_3_image_visualizer = ImageVisualizer.ImageVisualizer("Sobel 3 filter", self)
        self.sobel_3_image_visualizer.grid(row = 1, column = 1, padx = 3, pady = 3)

        self.sobel_5_button = tk.Button(self, text="Sobel 5", command=self.sobel_5)
        self.sobel_5_button.grid(row = 2, column = 1, sticky = "n")
        self.sobel_5_image_visualizer = ImageVisualizer.ImageVisualizer("Sobel 5 filter", self)
        self.sobel_5_image_visualizer.grid(row=3, column=1, padx=3, pady=3)

        self.prewitt_button = tk.Button(self, text="prewitt", command=self.prewitt)
        self.prewitt_button.grid(row=4, column=1, sticky="n")
        self.prewitt_image_visualizer = ImageVisualizer.ImageVisualizer("Prewitt filter", self)
        self.prewitt_image_visualizer.grid(row=5, column=1, padx=3, pady=3)

        self.thresholds_sliders = ContrastSliders.ContrastSliders(self.minmax_changed, self)
        self.thresholds_sliders.set_max(200)
        self.thresholds_sliders.set_min(50)
        self.thresholds_sliders.disable()
        self.thresholds_sliders.grid(row = 2, column = 0, sticky ="nw")
        self.canny_filter_button = tk.Button(self, text="Canny", command=self.canny)
        self.canny_filter_button.grid(row=2, column=0, sticky="n")
        self.canny_image_visualizer = ImageVisualizer.ImageVisualizer("Canny", self)
        self.canny_image_visualizer.grid(row=3, column=0, padx=3, pady=3, rowspan = 2)


    def choose_iconic_image_path(self):
        self.iconic_image_path = filedialog.askopenfilename()
        self.lab3.set_iconic_image(self.iconic_image_path)
        self.iconic_image = self.lab3.get_iconic_image()
        self.iconic_image_visualizer.set_image(self.iconic_image)
        self.thresholds_sliders.enable()

    def minmax_changed(self, new_value):
        state = self.thresholds_sliders.min_scale.cget("state")
        if (state == "active" or state == "normal"):
            self.canny()

    def sobel_3(self):
        try:
            self.sobel_3_image_visualizer.set_image(self.lab3.edge("sobel_3"))
        except Exception as e:
            self.sobel_3_image_visualizer.image_canvas.set_error_message(str(e))

    def sobel_5(self):
        try:
            self.sobel_5_image_visualizer.set_image(self.lab3.edge("sobel_5"))
        except Exception as e:
            self.sobel_5_image_visualizer.image_canvas.set_error_message(str(e))

    def canny(self):
        try:
            self.canny_image_visualizer.set_image(self.lab3.edge("canny", self.thresholds_sliders.get_min(), self.thresholds_sliders.get_max()))
        except Exception as e:
            self.canny_image_visualizer.image_canvas.set_error_message(str(e))

    def prewitt(self):
        try:
            self.prewitt_image_visualizer.set_image(self.lab3.edge("prewitt"))
        except Exception as e:
            self.prewitt_image_visualizer.image_canvas.set_error_message(str(e))
