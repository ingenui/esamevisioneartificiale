import threading

class ThreadedLab07Runner(threading.Thread):
    
    def __init__(self, queue, lab07):
        threading.Thread.__init__(self)
        self.lab07 = lab07
        self.queue = queue

    def run(self):
        self.lab07.run()
        self.queue.put("finished")

