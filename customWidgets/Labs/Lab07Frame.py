import tkinter as tk
from tkinter import filedialog, ttk
from tkinter.filedialog import askdirectory
import queue
from customWidgets import ImageVisualizer, RankingVisualizer

from Labs.Lab07 import lab07
from customWidgets.Labs import ThreadedDatasetLoader, ThreadedLab07Runner

class Lab07Frame(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "Laboratorio 7", padx = 5, pady = 5)
        self.columnconfigure(0, weight = 15)
        self.columnconfigure(1, weight = 15)
        self.columnconfigure(2, weight = 34)
        self.columnconfigure(3, weight = 34)
        self.columnconfigure(4, weight = 2)
        self.rowconfigure(0, weight = 2)
        self.rowconfigure(1, weight = 49)
        self.rowconfigure(2, weight = 1)
        self.rowconfigure(3, weight = 48)

        self.grid_propagate(0)
        self.grid(sticky = "nswe")
        self.lab07 = lab07.Lab7()
        self.measures = self.lab07.get_methods()
        self.retrieval_method = tk.StringVar(master=self)
        self.retrieval_method.set(self.measures[0])
        self.createWidgets()

    def createWidgets(self):
        self.choose_file_button = tk.Button(self, text = "Choose file", command = self.choose_test_image_path, state=tk.DISABLED)
        self.choose_file_button.grid(row = 0, column = 0, sticky = "n")

        self.choose_dataset_button = tk.Button(self, text="Choose dataset folder", command=self.choose_dataset_folder)
        self.choose_dataset_button.grid(row=0, column=1, sticky="n")

        self.test_image_visualizer = ImageVisualizer.ImageVisualizer("Original", self)
        self.test_image_visualizer.grid(row = 1, column = 0, padx = 3, pady = 3, columnspan = 2)

        self.retrieval_method_label = tk.Label(self, text="Misura di confronto:")
        self.retrieval_method_label.grid(row=2, column=0, sticky="nw")

        self.retrieval_method_choice = tk.OptionMenu(self, self.retrieval_method, *self.measures, command=lambda x: self.choose_test_retrieval_method())
        self.retrieval_method_choice.config(width=20)
        self.retrieval_method_choice.grid(row=3, column=0, sticky="nw")

        self.best_marginal_image_visualizer = ImageVisualizer.ImageVisualizer("Marginal best match", self)
        self.best_marginal_image_visualizer.grid(row=1, column=2, padx=3, pady=3, columnspan=2)

        self.best_joint_image_visualizer = ImageVisualizer.ImageVisualizer("Joint best match", self)
        self.best_joint_image_visualizer.grid(row=2, column=2, padx=3, pady=3, rowspan=2, columnspan = 2)

        self.marginal_ranking_visualizer = RankingVisualizer.RankingVisualizer("Marginal ranking", self)
        self.marginal_ranking_visualizer.grid(row=1, column=4, sticky="news")

        self.joint_ranking_visualizer = RankingVisualizer.RankingVisualizer("Joint ranking", self)
        self.joint_ranking_visualizer.grid(row=2, column=4, sticky="news", rowspan=2)

    def start_progress_bar(self):
        self.progress_bar = ttk.Progressbar(self, orient = "horizontal", mode = "indeterminate")
        self.progress_bar.grid(row = 0, column = 2, sticky = "we", columnspan = 3)
        self.progress_bar.start()

    def stop_progress_bar(self):
        if hasattr(self, "progress_bar") and not self.progress_bar is None:
            self.progress_bar.stop()
            self.progress_bar.grid_remove()

    def choose_test_image_path(self):
        self.test_image_path = filedialog.askopenfilename()
        if self.test_image_path == "":
            return
        self.lab07.set_test_image(self.test_image_path)
        self.test_image = self.lab07.get_test_image()
        self.test_image_visualizer.set_image(self.test_image)

        self.start_progress_bar()
        self.choose_file_button.config(state=tk.DISABLED)
        self.choose_dataset_button.config(state=tk.DISABLED)

        self.lab07_runner_queue = queue.Queue()
        ThreadedLab07Runner.ThreadedLab07Runner(self.lab07_runner_queue, self.lab07).start()
        self.master.after(100, self.process_lab07_runner_queue)

    def choose_test_retrieval_method(self):
        self.lab07.set_current_method(self.retrieval_method.get())
        if not hasattr(self, 'dataset_path') or (self.dataset_path == ''):
            return
        if not hasattr(self, 'test_image_path') or self.test_image_path == '':
            self.choose_test_image_path()
        else:
            self.start_progress_bar()
            self.choose_file_button.config(state=tk.DISABLED)
            self.choose_dataset_button.config(state=tk.DISABLED)

            self.lab07_runner_queue = queue.Queue()
            ThreadedLab07Runner.ThreadedLab07Runner(self.lab07_runner_queue, self.lab07).start()
            self.master.after(100, self.process_lab07_runner_queue)

    def process_lab07_runner_queue(self):
        try:
            self.lab07_runner_queue.get(0)
            self.stop_progress_bar()
            self.best_marginal_image_visualizer.set_image(self.lab07.get_rgb_marginal_best_match())
            self.best_joint_image_visualizer.set_image(self.lab07.get_rgb_joint_best_match())
            self.choose_file_button.config(state=tk.NORMAL)
            self.choose_dataset_button.config(state=tk.NORMAL)

            self.marginal_ranking_visualizer.add_ranking(self.lab07.get_marginal_results())
            self.joint_ranking_visualizer.add_ranking(self.lab07.get_joint_results())
        except queue.Empty:
            self.master.after(100, self.process_lab07_runner_queue)

    def choose_dataset_folder(self):
        self.dataset_path = askdirectory()
        if self.dataset_path == "":
            return
        self.start_progress_bar()
        
        self.dataset_loader_queue = queue.Queue()
        ThreadedDatasetLoader.ThreadedDatasetLoader(self.dataset_loader_queue, self.lab07, self.dataset_path).start()
        self.master.after(100, self.process_dataset_loader_queue)
        self.choose_dataset_button.config(state = tk.DISABLED)

    def process_dataset_loader_queue(self):
        try:
            self.dataset_loader_queue.get(0)
            self.stop_progress_bar()
            self.choose_file_button.config(state=tk.NORMAL)
            self.choose_dataset_button.config(state=tk.NORMAL)
        except queue.Empty:
            self.master.after(100, self.process_dataset_loader_queue)

    def noise_mean_changed(self, new_mean):
        state = self.noise_sliders.noise_mean_scale.cget("state")
        if (state == "active" or state == "normal"):
            self.update_noise_image()

    def noise_std_dev_changed(self, new_variance):
        state = self.noise_sliders.noise_std_dev_scale.cget("state")
        if (state == "active" or state == "normal"):
            self.update_noise_image()

    def update_noise_image(self):
        current_mean = self.noise_sliders.get_mean()
        current_std_dev = self.noise_sliders.get_std_dev()
        self.noised_image = self.hw2.get_rgb_noised_image(current_mean, current_std_dev)
        self.best_marginal_image_visualizer.set_image(self.noised_image)