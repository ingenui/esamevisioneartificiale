import threading

class ThreadedDatasetLoader(threading.Thread):
    def __init__(self, queue, lab07, dataset_path):
        threading.Thread.__init__(self)
        self.lab07 = lab07
        self.dataset_path = dataset_path
        self.queue = queue

    def run(self):
        self.lab07.set_dataset(self.dataset_path)
        self.queue.put("finished")


