import tkinter as tk

class EmptyPage(tk.LabelFrame):
    """description of class"""

    def __init__(self, master=None):
        tk.LabelFrame.__init__(self, master, text = "No homework or lab selected", padx = 3, pady = 3, width = 600, height = 600)
        self.grid_propagate(0)
        self.grid()