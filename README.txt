This software requires Python 3.6. You can download and install Python 3.6 from: https://www.python.org/downloads/release/python-367rc2/
This software requires Cuda 9.0. You can downlod and install it from this URL: https://developer.nvidia.com/cuda-toolkit
This software requires cuDNN 7. You can downlod and install it from this URL: https://developer.nvidia.com/rdp/cudnn-download

Run Windows PowerShell as administrator
To solve permission issues, run the following command and answer "yes":
	Set-ExecutionPolicy Unrestricted Process

Ensure you have virtualenv package installed, if not run:
	py -m pip install virtualenv

Create a new environment in the project folder (esamevisioneartificiale) and activate it:
	py -m virtualenv venv
	.\venv\Scripts\activate

Install required packages indicated in the requirements.txt file:
	pip install -r requirements.txt

Install shapely package
Download the correct version for your system from https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely and run the following command:
	py -m pip install  <.whl file path>	(Shapely-1.6.4.post1-cp36-cp36m-win32.whl or Shapely-1.6.4.post1-cp36-cp36m-win_amd64.whl)

Install tensorflow 1.5.0:
	pip install tensorflow==1.5.0

Download and install our modified version of darkflow from https://mega.nz/#F!yhZn2AzC!4Sx3_Ip7qX4qL7Ihd7nmAg.
Move to the download location and run the following command:
	pip install -e .\yolo_tracking\darkflow

Copy the downloaded file "yolov2.weights" into esamevisioneartificiale\Homeworks\Homework6\bin folder to have the default weights for pedestrian detection

Run the program:
	python main.py