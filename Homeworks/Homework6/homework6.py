import queue
import numbers
import os
from Homeworks.Homework6 import human_detector, vatic_parser
from Homeworks.Homework6 import human_tracker
import numpy as np
import cv2
from Homeworks.utils import ThreadedFrameGrabber
from shapely.geometry import box

class Homework6(object):
    default_model_path = './Homeworks/Homework6/cfg/yolov2.cfg'
    default_weights_path = './Homeworks/Homework6/bin/yolov2.weights'
    default_threshold = 0.3
    username = 'admin'
    password = '123456'
    protocol = 'http://'
    url = 'neuralstory-host.ing.unimore.it/camera/videostream.cgi'
    def __init__(self):
        self.frame_queue = queue.Queue()
        self.model_path = os.path.abspath(Homework6.default_model_path)
        self.weights_path = os.path.abspath(Homework6.default_weights_path)
        self.threshold = self.default_threshold
        self.labels_path = None
        self.labels = None
        self.lab_camera_address = Homework6.protocol + Homework6.username + ':' + Homework6.password + '@' + Homework6.url
        self.video_source = "lab"
        self.frame_number = 0
        self.humans = []
        self.spatial_overlap_percentages = []

    def set_model_path(self, model_path):
        if os.path.isfile(model_path):
            self.model_path = model_path

    def set_weights_path(self, weights_path):
        if os.path.isfile(weights_path):
            self.weights_path = weights_path

    def set_threshold(self, threshold):
        if isinstance(threshold, numbers.Number) and threshold > 0 and threshold <=1:
            self.threshold = threshold

    def set_labels_path(self, labels_path):
        if os.path.isfile(labels_path):
            self.labels_path = labels_path
            self.vp = vatic_parser.VaticParser(self.labels_path)
            self.labels = self.vp.get_humans_labels()

    '''
        If type == 0, load the DarkFlow human detector
        If type == 1, load the Viola&Jones human detector with Haar features 
    '''
    def load_detector(self, type=1):
        if type==0:
            self.detector = human_detector.DF_Human_Detector(self.model_path, self.weights_path, self.threshold)
        elif type==1:
            self.detector = human_detector.VJ_Human_Detector()
        elif type==2:
            self.detector = human_tracker.human_tracker(self.model_path, self.weights_path, self.threshold)
        else:
            print("Value of parameter type not acceptable")

    def set_video_source(self, video_source):
        if (video_source == "lab"):
            self.video_source = self.lab_camera_address
        else:
            self.video_source = video_source

    def start_video_capture(self):
        self.frame_grabber = ThreadedFrameGrabber.ThreadedFrameGrabber(self.frame_grabbed_callback, self.video_source)
        self.frame_queue.queue.clear()
        self.frame_grabber.start()

    def stop_video_capture(self):
        self.frame_grabber.stop()

    def frame_grabbed_callback(self, frame):
        #if self.frame_number > 2000:
        self.humans = self.detector.detect(frame)
        if not (self.labels_path is None):
            if self.frame_number > 2000:
                intersection = float(0)
                union = float(0)
                for person in self.labels:
                    if not (np.all(person['bounding_boxes'][self.frame_number - 1] == (0, 0))):
                        coordinates = person['bounding_boxes'][self.frame_number - 1]
                        tl = (coordinates[0][0], coordinates[0][1])
                        br = (coordinates[2][0], coordinates[2][1])
                        cv2.rectangle(frame, tl, br, (255, 0, 0), 1)
                        text = "Id: " + person['id'] + "  -  " + person['name']
                        frame = cv2.putText(frame, text, tl, cv2.FONT_HERSHEY_COMPLEX, 0.2, (0, 0, 0), 1)
                        #Performance evaluation
                        xm_label = float(coordinates[0][0] + coordinates[2][0]) / 2.
                        ym_label = float(coordinates[0][1] + coordinates[2][1]) / 2.
                        distances = []
                        rect_label = box(coordinates[0][0], coordinates[0][1], coordinates[2][0], coordinates[2][1])
                        if self.humans:
                            for human in self.humans:
                                xm_detection = float(human['topleft']['x'] + human['bottomright']['x']) / 2.
                                ym_detection = float(human['topleft']['y'] + human['bottomright']['y']) / 2.
                                distances.append((xm_label-xm_detection)**2 + (ym_label-ym_detection)**2)
                            human_index = np.argmin(distances)
                            rect_prediction = box(self.humans[human_index]['topleft']['x'],
                                                  self.humans[human_index]['bottomright']['y'],
                                                  self.humans[human_index]['bottomright']['x'],
                                                  self.humans[human_index]['topleft']['y'])
                            intersection += rect_label.intersection(rect_prediction).area
                            del distances[human_index]
                        union += rect_label.area
                self.spatial_overlap_percentages.append(intersection/union)
                print("Overlapping medio: " + str(np.mean(self.spatial_overlap_percentages)))
                print("Overlapping min: " + str(np.min(self.spatial_overlap_percentages)))
                print("Overlapping max: " + str(np.max(self.spatial_overlap_percentages)))
                print("Overlapping variance: " + str(np.var(self.spatial_overlap_percentages)))
        self.frame_queue.put(frame)
        self.frame_number += 1

    def get_next_frame(self):
        try:
            return self.frame_queue.get()
        except:
            return None