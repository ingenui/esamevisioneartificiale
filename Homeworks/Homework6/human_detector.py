import cv2
import sys
import os
from darkflow.net.build import TFNet
import numpy as np

class Human_Detector(object):
    def __init__(self):
        self.classifier = None
        self.humans = []
        self.person_templates = []
        self.name = None
        self.version = None

    def detect(self, image):
        pass
        # image_gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        # self.humans = self.classifier.detectMultiScale(image_gray)
        # for (x, y, w, h) in self.humans:
        #     cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
        # return self.humans

    # Non testata!!!
    def tracking(self, frame):
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        for (x, y, w, h) in self.humans:
            self.person_templates.append(frame_gray[y:y + h, x:x + w])
        while True:
            response = cv2.matchTemplate(frame_gray, face_template, cv2.TM_CCORR_NORMED)
            minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(response)
            cv2.rectangle(frame, maxLoc, (maxLoc[0] + face_template.shape[0], maxLoc[1] + face_template.shape[1]),
                          (255, 0, 0), 2)
            face_template = frame_gray[maxLoc[1]: maxLoc[1] + face_template.shape[0],
                            maxLoc[0]: maxLoc[0] + face_template.shape[1]]
            cv2.imshow("Ciao", frame)
            cv2.waitKey(0)

    def stupid_tracking(self):
        video = cv2.VideoCapture("Buffy.mp4")
        if not video.isOpened():
            print("Could not open video")
            sys.exit()

        ok, frame = video.read()
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        if not ok:
            print ('Cannot read video file')
            sys.exit()
        faces = self.detect(frame_gray)
        x,y,w,h = faces[1]
        face_template = frame_gray[y:y+h, x:x+w]

        while True:
            ok, frame = video.read()
            if not ok:
                break
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            response = cv2.matchTemplate(frame_gray, face_template, cv2.TM_CCORR_NORMED)
            minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(response)
            cv2.rectangle(frame, maxLoc, (maxLoc[0]+face_template.shape[0], maxLoc[1]+face_template.shape[1]), (255, 0, 0), 2)
            face_template = frame_gray[maxLoc[1]: maxLoc[1]+face_template.shape[0], maxLoc[0]: maxLoc[0]+face_template.shape[1]]
            cv2.imshow("Ciao", frame)
            cv2.waitKey(0)


class VJ_Human_Detector(Human_Detector):
    def __init__(self):
        Human_Detector.__init__(self)
        self.name = "Viola&Jones detector"
        self.classifier = cv2.CascadeClassifier(
            "C:/Users/picio/Google Drive - 188357/Quinto anno/Visione Artificiale/Esame/esamevisioneartificiale/Homeworks/Homework6/Full_body.xml")

    def detect(self, image):
        print("VJ Human detection")
        image_gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        self.humans = self.classifier.detectMultiScale(image_gray)
        for (x, y, w, h) in self.humans:
            cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
        return self.humans

    #Non testata!!!
    def tracking(self, frame):
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        for (x, y, w, h) in self.humans:
            self.person_templates.append(frame_gray[y:y + h, x:x + w])
        while True:
            response = cv2.matchTemplate(frame_gray, person_template, cv2.TM_CCORR_NORMED)
            minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(response)
            cv2.rectangle(frame, maxLoc, (maxLoc[0] + person_template.shape[0], maxLoc[1] + person_template.shape[1]),
                          (255, 0, 0), 2)
            person_template = frame_gray[maxLoc[1]: maxLoc[1] + person_template.shape[0],
                            maxLoc[0]: maxLoc[0] + person_template.shape[1]]
            # cv2.imshow("Ciao", frame)
            # cv2.waitKey(0)

    def stupid_tracking(self):
        pass

class DF_Human_Detector(Human_Detector):
    def __init__(self, model_path, weights_path, threshold):
        Human_Detector.__init__(self)
        self.name = "Darkflow Detector"
        self.version = os.path.basename(model_path)
        self.tfnet_option = {
            'model': model_path,
            'load': weights_path,
            'threshold': threshold,
            'gpu': 0.8
        }
        self.tfnet = TFNet(self.tfnet_option)

    def detect(self, image):
        self.humans = self.tfnet.return_predict(image)
        colors = [tuple(255 * np.random.rand(3)) for i in range(5)]
        for color, human in zip(colors, self.humans):
            tl = (human['topleft']['x'], human['topleft']['y'])
            br = (human['bottomright']['x'], human['bottomright']['y'])
            label = human['label']
            cv2.rectangle(image, tl, br, color, 2)
            cv2.putText(image, label, tl, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2)
        return self.humans

    # Non testata!!!
    def tracking(self, frame):
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        for (x, y, w, h) in self.humans:
            self.person_templates.append(frame_gray[y:y + h, x:x + w])
        while True:
            response = cv2.matchTemplate(frame_gray, person_template, cv2.TM_CCORR_NORMED)
            minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(response)
            cv2.rectangle(frame, maxLoc, (maxLoc[0] + person_template.shape[0], maxLoc[1] + person_template.shape[1]),
                          (255, 0, 0), 2)
            person_template = frame_gray[maxLoc[1]: maxLoc[1] + person_template.shape[0],
                              maxLoc[0]: maxLoc[0] + person_template.shape[1]]
            # cv2.imshow("Ciao", frame)
            # cv2.waitKey(0)

    def stupid_tracking(self):
        pass



