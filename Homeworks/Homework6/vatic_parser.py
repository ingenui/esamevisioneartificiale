import xml.etree.ElementTree as ET
import numpy as np


class VaticParser():
    def __init__(self, xml_path, first_frame=0):
        try:
            self.annotation_tree = ET.parse(xml_path)
        except Exception as e:
            print("Errore nell'apertura del file xml: " + str(e))
        if not (self.annotation_tree.getroot()[0].text == 'not available'):
            self.folder = None
        if not (self.annotation_tree.getroot()[1].text == 'not available'):
            self.filename = None
        type = self.annotation_tree.getroot()[2][0].text
        source_image = self.annotation_tree.getroot()[2][1].text
        source_annotation = self.annotation_tree.getroot()[2][2].text
        self.source = (type, source_image, source_annotation)
        self.start_frame = int(self.annotation_tree.getroot()[3][6].text)
        self.end_frame = int(self.annotation_tree.getroot()[3][7].text)
        self.humans_number = 0
        self.humans = []
        for obj in self.annotation_tree.getroot().findall('object'):
            self.human = {}
            self.human['name'] = obj[0].text
            self.human['moving'] = obj[1].text
            self.human['verified'] = obj[3].text
            self.human['id'] = obj[4].text
            self.human['created_frame'] = obj[5].text
            if not (int(obj[6].text) == self.start_frame):
                raise ValueError('Invalid starting fram value read')
            if not (int(obj[7].text) == self.end_frame):
                raise ValueError('Invalid ending fram value read')
            self.human['bounding_boxes'] = np.zeros([self.end_frame - first_frame, 4, 2], dtype=np.uint16)
            for polygon in obj.findall('polygon'):
                if int(polygon[0].text) >= first_frame:
                    hl_vertex = np.asarray([np.uint16(polygon[1][0].text), np.uint16(polygon[1][1].text)], dtype=np.uint16)
                    bl_vertex = np.asarray([np.uint16(polygon[2][0].text), np.uint16(polygon[2][1].text)], dtype=np.uint16)
                    br_vertex = np.asarray([np.uint16(polygon[3][0].text), np.uint16(polygon[3][1].text)], dtype=np.uint16)
                    hr_vertex = np.asarray([np.uint16(polygon[4][0].text), np.uint16(polygon[4][1].text)], dtype=np.uint16)
                    self.human['bounding_boxes'][int(polygon[0].text) - first_frame - 1, :, :] = np.array([hl_vertex, bl_vertex, br_vertex, hr_vertex], dtype=np.uint16)
            self.humans.append(self.human)
            self.humans_number += 1

    def get_humans_labels(self):
        return self.humans
