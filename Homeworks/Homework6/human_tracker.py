from darkflow.net.build import TFNet
from Homeworks.Homework6 import human_detector
import os
import numpy as np
import cv2

class human_tracker(human_detector.Human_Detector):
    def __init__(self, model_path, weights_path, threshold):
        human_detector.Human_Detector.__init__(self)
        self.name = "Darkflow Detector"
        self.version = os.path.basename(model_path)
        self.tfnet_option = {
            'model': model_path,
            'load': weights_path,
            'threshold': threshold,
            'gpu': 0.8,
            'track': True,
            'tracker': "sort",
            'trackObj': "person",
			'display': False
        }
        self.tfnet = TFNet(self.tfnet_option)

    def detect(self, image):
        #self.humans = self.tfnet.return_predict(image)
        self.humans = self.tfnet.return_track_predict(image)
        color = (0, 255, 0)
        for human in self.humans:
            tl = (human['topleft']['x'], human['topleft']['y'])
            br = (human['bottomright']['x'], human['bottomright']['y'])
            id = human['id']
            cv2.rectangle(image, tl, br, color, 2)
            cv2.putText(image, id, tl, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2)
        return self.humans


