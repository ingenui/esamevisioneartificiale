import cv2
import numpy as np


class homework2(object):

    def __init__(self):
        self.iconic_image = None
        self.noised_image = None

    def load_iconic_image(self, image_path):
        self.iconic_image = cv2.imread(image_path, cv2.IMREAD_COLOR)       
    
    def get_rgb_iconic_image(self):
        if self.iconic_image is None:
            raise Exception("Must load iconic image")
        return cv2.cvtColor(self.iconic_image, cv2.COLOR_BGR2RGB)

    def get_rgb_noised_image(self, mean, std_dev):        
        self.get_noised_image(mean, std_dev)

        return cv2.cvtColor(self.noised_image, cv2.COLOR_BGR2RGB)

    def get_noised_image(self, mean, std_dev):
        if self.iconic_image is None:
            raise Exception("Must load iconic image before noising")
        mean_vector = np.asarray([np.abs(mean), np.abs(mean), np.abs(mean)])
        std_dev = np.asarray([std_dev, std_dev, std_dev])
        noise = np.empty_like(self.iconic_image).astype(np.double)
        cv2.randn(noise, mean_vector, std_dev)
        if mean<0:
            noise = -noise
        self.noised_image = (self.iconic_image.astype(np.double) + noise)
        self.noised_image[self.noised_image > 255] = 255
        self.noised_image[self.noised_image < 0] = 0
        self.noised_image = self.noised_image.astype(np.uint8)