class FoundObject(object):
    
    def __init__(self, name, bounding_box):
        self.name = name
        self.bounding_box = bounding_box

    def get_name(self):
        return self.name

    def get_bounding_box(self):
        return self.bounding_box

