import cv2
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from Homeworks.Homework4 import FoundObject


class homework4(object):
    def __init__(self):
        self.original_image=None        

    def set_original_image(self, img):
        if not(img is None):
            self.original_image = img
            if len(self.original_image.shape)==3:
                self.grayscale_original_image = cv2.cvtColor(self.original_image, cv2.COLOR_BGR2GRAY)
            else:
                self.grayscale_original_image = self.original_image

    def get_original_image(self):
        return self.original_image

    def get_component_pixels_array(self, component_image):        
        component_pixels = np.where(component_image != 0)
        points = []
        for x, y in zip(component_pixels[1], component_pixels[0]):
            points.append((x, y))
        # Run PCA algorithm on the component pixels and transform them with new coordinates
        self.pca = PCA(n_components=2, svd_solver='full', tol=0.0, whiten=False, random_state=None, iterated_power='auto')
        new_coords_points = self.pca.fit_transform(points)
        # Calculate the center coordinates as the origin's inverse transform
        x_center, y_center = self.pca.inverse_transform([0,0])
        # Calculate the new axes length
        range_1_max = max(new_coords_points[:, 0])
        range_1_min = min(new_coords_points[:, 0])
        range_2_max = max(new_coords_points[:, 1])
        range_2_min = min(new_coords_points[:, 1])

        # Calculate vertices of the component's bounding box and put the polygon on the color image
        vectors = np.empty([4,2], dtype=np.float)
        vectors[0, :] = (self.pca.components_[1] * range_2_max).astype(np.int)
        vectors[1, :] = (self.pca.components_[1] * range_2_min).astype(np.int)
        vectors[2, :] = (self.pca.components_[0] * range_1_max).astype(np.int)
        vectors[3, :] = (self.pca.components_[0] * range_1_min).astype(np.int)
        vertices = np.ndarray((4,2), dtype=np.int32)
        vertices[0, :] = ([x_center, y_center] + vectors[0] + vectors[2])
        vertices[1, :] = ([x_center, y_center] + vectors[0] + vectors[3])
        vertices[2, :] = ([x_center, y_center] + vectors[1] + vectors[3])
        vertices[3, :] = ([x_center, y_center] + vectors[1] + vectors[2])
        return vertices

    def run_pipeline(self, adaptive_threshold_window_size, c_constant, adaptive_threshold_method_str, open_kernel_size, close_kernel_size, 
                     open_check, close_check, connectivity, percentage_threshold):
        if (adaptive_threshold_method_str == "mean"):
            adaptive_threshold_method = cv2.ADAPTIVE_THRESH_MEAN_C
        else:
            adaptive_threshold_method = cv2.ADAPTIVE_THRESH_GAUSSIAN_C

        self.segmented_image = cv2.adaptiveThreshold(self.grayscale_original_image, 255, 
                                                     adaptive_threshold_method, cv2.THRESH_BINARY_INV, 
                                                     adaptive_threshold_window_size, c_constant)

        
        open_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (open_kernel_size, open_kernel_size))
        close_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (close_kernel_size, close_kernel_size))

        self.morphology_image = self.segmented_image

        if open_check:
            self.morphology_image = cv2.morphologyEx(self.morphology_image, cv2.MORPH_OPEN, open_kernel)
        if close_check:
            self.morphology_image = cv2.morphologyEx(self.morphology_image, cv2.MORPH_CLOSE, close_kernel)

        ret_val, self.labels, stats, centroids = cv2.connectedComponentsWithStats(self.morphology_image, connectivity = connectivity)
        
        areas = []        
        for area in stats[:, cv2.CC_STAT_AREA]:
            areas.append([len(areas), area])
        
        areas = np.asarray(sorted(areas, key = lambda area: area[1], reverse = True))
        #biggest area is background, so skip it
        max_area = areas[1, 1]
        stop_index = 1        
        for index in range(2, len(areas)):
            if areas[index, 1] / max_area < percentage_threshold:
                break
            stop_index = index

        big_enough_areas = areas[1:stop_index + 1]

        self.big_enough_label_images = []
        for big_enough_area in big_enough_areas:
            big_enough_label = big_enough_area[0]
            big_enough_label_image = np.zeros_like(self.labels)
            big_enough_label_image[self.labels == big_enough_label] = big_enough_label
            self.big_enough_label_images.append(big_enough_label_image)

        all_big_enough_labels_image = np.zeros_like(self.labels)
        for big_enough_labels_image in self.big_enough_label_images:
            all_big_enough_labels_image = np.add(big_enough_labels_image, all_big_enough_labels_image)
        label_hue = np.uint8(179 * all_big_enough_labels_image / np.max(all_big_enough_labels_image))
        blank_ch = 255 * np.ones_like(label_hue)
        labeled_img = cv2.merge([label_hue, blank_ch, blank_ch])
        self.connected_components_color_image = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)
        self.connected_components_color_image[label_hue==0] = 0

        self.found_objects = []
        for big_enough_labels_image in self.big_enough_label_images:
            bbox = self.get_component_pixels_array(big_enough_labels_image)
            object_name = "Object " + str(len(self.found_objects))
            self.found_objects.append(FoundObject.FoundObject(object_name, bbox))

    def get_segmented_image(self):
        if hasattr(self, "segmented_image"):
            return self.segmented_image
        else:
            return None

    def get_morphology_image(self):
        if hasattr(self, "morphology_image"):
            return self.morphology_image
        else:
            None

    def get_found_objects(self):
        if hasattr(self, "found_objects"):
            return self.found_objects
        else:
            return None

    def get_found_object_by_name(self, name):
        if hasattr(self, "found_objects"):
            for object in self.found_objects:
                if object.name == name:
                    return object
            return None
        else:
            return None

    def get_connected_components_image(self):
        if hasattr(self, "connected_components_color_image"):
            return self.connected_components_color_image
        else:
            return None

    def get_connected_components_labels_images(self):
        if hasattr(self, "big_enough_label_images"):
            return self.big_enough_label_images
        else:
            return None

    def magnitude(self, grad_x, grad_y):
        return np.sqrt(grad_x ** 2 + grad_y ** 2)

    def edges(self, filter_dimension=3):
        sobelx = np.array(([-1, 0, 1], [-2, 0, 2], [-1, 0, 1]), dtype=np.float32)
        sobely = np.array(([1, 2, 1], [0, 0, 0], [-1, -2, -1]), dtype=np.float32)
        grad_x = cv2.filter2D(self.original_image, cv2.CV_32F, sobelx)
        grad_y = cv2.filter2D(self.original_image, cv2.CV_32F, sobely)

        max_value = np.sqrt((255 * 3) ** 2 + (255 * 3) ** 2).astype(np.float)
        M = self.magnitude(grad_x, grad_y) / max_value
        M *= 255
        th2 = cv2.adaptiveThreshold(self.grayscale_original_image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
        return th2