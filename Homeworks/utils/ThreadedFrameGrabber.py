import threading
import time
import cv2
import queue
import os
import numpy as np
from Homeworks.Homework6 import vatic_parser
from urllib import parse


class ThreadedFrameGrabber(threading.Thread):

    def __init__(self, callback, video_capture_id = 0, frame_rate = 15.0):
        self.frame_number = 0
        threading.Thread.__init__(self)
        if callback is None:
            raise ValueError("callback must not be None")
        self.can_grab = True
        parsed_url = parse.urlparse(video_capture_id)
        if bool(parsed_url.scheme):
            self.dir_source = False
        if os.path.isdir(video_capture_id):
            self.dir_source = True
        else: self.dir_source = False
        self.video_capture_id = video_capture_id
        self.sleep_time = 1.0 / float(frame_rate)
        self.callback = callback

    def stop(self):
        self.can_grab = False

    def run(self):
        if not self.dir_source:
            self.video_capture = cv2.VideoCapture(self.video_capture_id)
            #self.video_capture.set(2, 1700);
            while self.can_grab and self.video_capture.isOpened():
                ret, frame = self.video_capture.read()
                if ret != False:
                    self.frame_number +=1
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    if not self.callback is None:
                        self.callback(frame)
                time.sleep(self.sleep_time)
            self.video_capture.release()
        else:
            images = []
            for f in os.listdir(self.video_capture_id):
                if f.endswith("jpg"):
                    images.append(f)
            for image in images:
                image_path = os.path.join(self.video_capture_id, image)
                frame = cv2.imread(image_path)
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                if (cv2.waitKey(1) & 0xFF) == ord('q'):  # Hit `q` to exit
                    break
                self.callback(frame)
                self.frame_number += 1
        print("image count: " + str(self.frame_number))