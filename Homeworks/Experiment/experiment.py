import cv2
import queue
from Homeworks.utils import ThreadedFrameGrabber
import numpy as np

class experiment(object):
    def __init__(self):

        self.frame_queue = queue.Queue()

    def start_video_capture(self):
        self.frame_grabber = ThreadedFrameGrabber.ThreadedFrameGrabber(self.frame_grabbed_callback)
        self.frame_queue.queue.clear()
        self.frame_grabber.start()

    def stop_video_capture(self):
        self.frame_grabber.stop()

    def frame_grabbed_callback(self, frame):
        gradient = cv2.Canny(frame, 50, 200)
        self.frame_queue.put(gradient)

    def get_next_frame(self):
        try:
            return self.frame_queue.get()
        except:
            return None