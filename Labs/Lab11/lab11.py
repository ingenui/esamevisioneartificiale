import cv2
import numpy as np

class lab11(object):
    """description of class"""

    def __init__(self, **kwargs):        
        self.previous_frame = None

    def release_camera(self):
        if not self.cap is None:
            self.cap.release()

    def get_current_optical_flow(self):
        if self.previous_frame is None:
            self.cap = cv2.VideoCapture(0)
            ret, self.previous_frame = self.cap.read()
            self.hsv = np.zeros_like(self.previous_frame)
            self.hsv[:,:,1] = 255

            self.previous_frame = cv2.cvtColor(self.previous_frame, cv2.COLOR_BGR2GRAY)            
            return np.zeros_like(self.previous_frame)

        ret, current_frame = self.cap.read()
        current_frame = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)

        flow = cv2.calcOpticalFlowFarneback(self.previous_frame, current_frame, None, 0.5, 3, 15, 3, 5, 1.2, 0)

        mag, ang = cv2.cartToPolar(flow[:, :, 0], flow[:, :, 1])

        self.hsv[:, :, 0] = ang * 180/np.pi/2
        self.hsv[:, :, 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        rgb = cv2.cvtColor(self.hsv, cv2.COLOR_HSV2RGB)

        self.previous_frame = current_frame

        return rgb