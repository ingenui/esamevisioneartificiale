import numpy as np
import cv2
import os


class Lab7:
    standard_width = 200
    standard_heigh = 200
    methods = [(cv2.HISTCMP_CORREL, "Correlation"),
               (cv2.HISTCMP_CHISQR, "Chi-square"),
               (cv2.HISTCMP_INTERSECT, "Intersection"),
               (cv2.HISTCMP_BHATTACHARYYA, "BHATTACHARYYA")]

    def __init__(self):
        self.currentMethod = cv2.HISTCMP_CORREL

    def get_methods(self):
        l = []
        for m in self.methods:
            l.append(m[1])
        return np.asarray(l)

    def set_current_method(self, method):
        for k, name in self.methods:
            if name==method:
                self.currentMethod = k
                break

    def set_test_image(self, test_image_path):
        """ Sets the test image to retrieve    """
        self.test_image_path = test_image_path
        self.test_image = cv2.imread(test_image_path)

    def set_dataset(self, dataset_path):
        """Sets the dataset for retrieval"""
        self.original_images = []
        self.MarginalHistograms = []
        self.JointHistograms = []
        for file in os.listdir(dataset_path):
            self.original_images.append((cv2.imread(dataset_path + "/" + file, 1), file, dataset_path+"/"+file))
        self.resizedImages = []
        for image, name, image_path in self.original_images:
            self.resizedImages.append(
                (cv2.resize(image, (self.standard_width, self.standard_heigh), interpolation=cv2.INTER_CUBIC), name, image_path))
        for image, name, image_path in self.resizedImages:
            BHist = np.asarray(cv2.calcHist([image], [0], None, [256], [0, 256]))
            GHist = np.asarray(cv2.calcHist([image], [1], None, [256], [0, 256]))
            RHist = np.asarray(cv2.calcHist([image], [2], None, [256], [0, 256]))
            self.MarginalHistograms.append((np.concatenate((BHist, GHist, RHist), 0), name, image_path))
            self.JointHistograms.append((self.__get_joint_histogram(image), name, image_path))


    def __get_hist(self, img, bins=256, normalized=True):
        """Return the frequency color histogram of a single channel image"""
        map = np.zeros(bins, dtype=np.int32)
        for (x, y), pixel in np.ndenumerate(img):
            map[int(pixel / (256. / float(bins)))] += 1
        if normalized:
            npixel = img.shape[0] * img.shape[1]
            normalizedMap = [x / npixel for x in map]
            return np.ndarray(normalizedMap)
        else:
            return map

    def __get_joint_histogram(self, img, binsB=256, binsG=256, binsR=256):
        """Returns the 3-dimensional normalized histogram of an image"""
        map = np.zeros((binsB, binsG, binsR), dtype=np.float32)
        for x in np.arange(img.shape[0]):
            for y in np.arange(img.shape[1]):
                b = img[x, y, 0]
                g = img[x, y, 1]
                r = img[x, y, 2]
                map[int(b / (256. / float(binsB))),
                    int(g / (256. / float(binsG))),
                    int(r / (256. / float(binsR)))] += 1
        npixel = img.shape[0] * img.shape[1]
        return map / np.float32(npixel)

    def get_marginal_results(self):
        return self.marginal_results

    def get_joint_results(self):
        return self.joint_results

    def get_rgb_marginal_best_match(self):
        return cv2.cvtColor(cv2.imread(self.best_marginal_match[2], 1), cv2.COLOR_BGR2RGB)

    def get_rgb_joint_best_match(self):
        return cv2.cvtColor(cv2.imread(self.best_joint_match[2], 1), cv2.COLOR_BGR2RGB)

    def get_test_image(self):
        if self.test_image is None:
            raise Exception("Must load test image")
        return cv2.cvtColor(self.test_image, cv2.COLOR_BGR2RGB)

    def __sort(self, results):
        if self.currentMethod == cv2.HISTCMP_BHATTACHARYYA:
            return sorted(results, key=lambda results: results[0])
        else:
            return sorted(results, key=lambda results: results[0], reverse=True)

    def run(self):
        """Computes the marginal and joint histograms for test image, and compares it with marginal and joint dataset histograms
         Sets the best_marginal_match and best_joint_match attributes"""
        self.test_image = cv2.imread(self.test_image_path, 1)
        self.test_image = cv2.resize(self.test_image, (Lab7.standard_width, Lab7.standard_heigh), interpolation=cv2.INTER_CUBIC)
        BHist = np.asarray(cv2.calcHist([self.test_image], [0], None, [256], [0, 256]))
        GHist = np.asarray(cv2.calcHist([self.test_image], [1], None, [256], [0, 256]))
        RHist = np.asarray(cv2.calcHist([self.test_image], [2], None, [256], [0, 256]))
        Test_marginal_histogram = np.concatenate((BHist, GHist, RHist), 0)
        Test_joint_histogram = self.__get_joint_histogram(self.test_image)

        self.marginal_results=[]
        for h, image_name, image_path in self.MarginalHistograms:
            self.marginal_results.append((cv2.compareHist(Test_marginal_histogram, h, self.currentMethod), image_name, image_path))
        self.marginal_results = self.__sort(self.marginal_results)
        self.best_marginal_match = self.marginal_results[0]

        self.joint_results=[]
        for h, image_name, image_path in self.JointHistograms:
            self.joint_results.append((cv2.compareHist(Test_joint_histogram, h, self.currentMethod), image_name, image_path))
        self.joint_results = self.__sort(self.joint_results)
        self.best_joint_match = self.joint_results[0]