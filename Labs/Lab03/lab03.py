import cv2
import numpy as np

class Lab3:
    sobel_3_x = np.array(([-1, 0, 1], [-2, 0, 2], [-1, 0, 1]), dtype=np.float32)
    sobel_3_y = np.array(([1, 2, 1], [0, 0, 0], [-1, -2, -1]), dtype=np.float32)
    sobel_5_x = np.array(([-1, -2, 0, 2, 1], [-2, -3, 0, 3, 2], [-3, -5, 0, 5, 3], [-2, -3, 0, 3, 2], [-1, -2, 0, 2, 1]), dtype=np.float32)
    sobel_5_y = np.array(([1, 2, 3, 2, 1], [2, 3, 5, 3, 2], [0, 0, 0, 0, 0], [-2, -3, -5, -3, -2], [-1, -2, -3, -2, -1]), dtype=np.float32)
    prewitt_x = np.array(([-1, 0, 1], [-1, 0, 1], [-1, 0, 1]), dtype=np.float32)
    prewitt_y = np.array(([1, 1, 1], [0, 0, 0], [-1, -1, -1]), dtype=np.float32)

    max_value = np.sqrt((255 * 3) ** 2 + (255 * 3) ** 2).astype(np.float)

    def __init__(self, iconic_image=None):
        self.iconic_image = iconic_image

    def contrast_stretching(self, img, min_new, max_new):
        if min_new > max_new:
            raise Exception("Max must be > than Min")
        self.min_old = np.amin(img)
        self.max_old = np.amax(img)
        img_new = np.zeros((img.shape))
        for (x, y), pixel in np.ndenumerate(img):
            img_new[x, y] = min_new + (pixel - self.min_old) * float(max_new - min_new) / float(self.max_old - self.min_old)
        img_new = np.around(img_new)
        img_new = img_new.astype(np.uint8)
        return img_new

    def manual_convolution(self, img, kernel):
        kh = kernel.shape[0]
        kw = kernel.shape[1]
        image_h = img.shape[0]
        image_w = img.shape[1]
        out_img = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
        for i in range((kh - 1) // 2, image_h - (kh - 1) // 2):
            for j in range((kw - 1) // 2, image_w - (kw - 1) // 2):
                out_img[i, j] = 0;
                out_img[i, j] += np.sum(
                    img[i - (kh - 1) // 2: i + (kh - 1) // 2 + 1, j - (kw - 1) // 2: j + (kw - 1) // 2 + 1] * kernel);
        out_img[out_img > 255] = 255
        out_img[out_img < 0] = 0
        return out_img

    def color_range(self):
        if self.iconic_image is None:
            raise Exception("Must load test image")
        return np.amin(self.iconic_image), np.amax(self.iconic_image)

    def get_iconic_image(self):
        if self.iconic_image is None:
            raise Exception("Must load test image")
        return self.iconic_image

    def set_iconic_image(self, iconic_image_path):
        self.iconic_image_path = iconic_image_path
        self.iconic_image = cv2.imread(iconic_image_path, cv2.IMREAD_GRAYSCALE)

    def edge(self, edge_operator, low_threshold=0, high_threshold=0):
        if edge_operator == "canny":
            edge = cv2.Canny(self.iconic_image, low_threshold, high_threshold)
            return edge
        if edge_operator == 'sobel_3':
            filter_x = Lab3.sobel_3_x
            filter_y = Lab3.sobel_3_y
        elif edge_operator == 'sobel_5':
            filter_x = Lab3.sobel_5_x
            filter_y = Lab3.sobel_5_y
        elif edge_operator == 'prewitt':
            filter_x = Lab3.prewitt_x
            filter_y = Lab3.prewitt_y
        grad_x = cv2.filter2D(self.iconic_image, cv2.CV_32F, filter_x)
        grad_y = cv2.filter2D(self.iconic_image, cv2.CV_32F, filter_y)
        M = np.sqrt(grad_x ** 2 + grad_y ** 2) / Lab3.max_value
        M *= 255
        teta = np.arctan2(grad_y, grad_x) + np.pi
        teta = teta / (2 * np.pi) * 180
        edge = np.zeros((M.shape[0], M.shape[1], 3), dtype=np.float32)
        edge[:, :, 0] = teta
        edge[:, :, 1] = 255
        edge[:, :, 2] = M
        edge = cv2.cvtColor(edge.astype(np.uint8), cv2.COLOR_HSV2BGR)
        return edge

if __name__ == "__main__":
    lab3 = Lab3()
    lab3.set_iconic_image("C:/Users/picio/Google Drive - 188357/Quinto anno/Visione Artificiale/Laboratorio/2018-03-19 Laboratorio/modena_skyline_03.png")
    cv2.imshow("Canny", lab3.edge("canny", 50, 200))
    cv2.waitKey()
    cv2.imshow("Canny", lab3.edge("canny", 200, 10))
    cv2.waitKey()
    cv2.imshow("Sobel 3", lab3.edge("sobel_3"))
    cv2.waitKey()
    cv2.imshow("Sobel 5", lab3.edge("sobel_5"))
    cv2.waitKey()
    cv2.imshow("Prewitt", lab3.edge("prewitt"))
    cv2.waitKey()