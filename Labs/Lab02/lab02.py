import cv2
import numpy as np

class Lab2:
    def __init__(self, iconic_image=None):
        self.iconic_image = iconic_image

    def contrast_stretching(self, img, min_new, max_new):
        if min_new > max_new:
            raise Exception("Max must be > than Min")
        self.min_old = np.amin(img)
        self.max_old = np.amax(img)
        img_new = np.zeros((img.shape))
        for (x, y), pixel in np.ndenumerate(img):
            img_new[x, y] = min_new + (pixel - self.min_old) * float(max_new - min_new) / float(self.max_old - self.min_old)
        img_new = np.around(img_new)
        img_new = img_new.astype(np.uint8)
        return img_new

    def manual_convolution(self, img, kernel):
        kh = kernel.shape[0]
        kw = kernel.shape[1]
        image_h = img.shape[0]
        image_w = img.shape[1]
        out_img = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
        for i in range((kh - 1) // 2, image_h - (kh - 1) // 2):
            for j in range((kw - 1) // 2, image_w - (kw - 1) // 2):
                out_img[i, j] = 0;
                out_img[i, j] += np.sum(
                    img[i - (kh - 1) // 2: i + (kh - 1) // 2 + 1, j - (kw - 1) // 2: j + (kw - 1) // 2 + 1] * kernel);
        out_img[out_img > 255] = 255
        out_img[out_img < 0] = 0
        return out_img

    def median_blur(self, img, window_height, window_width):
        if (window_width > 0 and window_height > 0):
            h = img.shape[0]
            w = img.shape[1]
            out_img = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
            for i in range((window_height - 1) // 2, h - (window_height - 1) // 2):
                for j in range((window_width - 1) // 2, w - (window_width - 1) // 2):
                    out_img[i, j] = np.median(img[i - (window_height - 1) // 2: i + (window_height - 1) // 2 + 1,
                                              j - (window_width - 1) // 2: j + (window_width - 1) // 2 + 1])
            out_img[out_img > 255] = 255
            out_img[out_img < 0] = 0
            return out_img
        else:
            print("Invalid dimension")

    def bilateral(self, img, k, l, sigmad, sigmar):
        if (k > 0 and l > 0 and sigmad > 0 and sigmar > 0):
            img = img.astype(np.float)
            h = img.shape[0]
            w = img.shape[1]
            out_img = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
            m, n = np.indices((k, l))
            m -= (k - 1) // 2
            n -= (l - 1) // 2
            domain_k = (-(m ** 2 + n ** 2) / (2 * sigmad ** 2)) #Costruzione del filtro relativo alla distanza euclidea tra pixel
            for i in range((k - 1) // 2, h - (k - 1) // 2):
                for j in range((l - 1) // 2, w - (l - 1) // 2):
                    filter = np.exp(domain_k - (img[i, j] - img[i + m, j + n]) ** 2 / (2 * sigmar ** 2)) # W(i,j,k,l)
                    out_img[i, j] = int(np.sum(img[i - (k - 1) // 2: i + (k - 1) // 2 + 1,
                                           j - (l - 1) // 2: j + (l - 1) // 2 + 1] * filter) / np.sum(filter))
            out_img[out_img > 255] = 255
            out_img[out_img < 0] = 0
            return out_img
        else:
            print("Invalid dimension")
            return img

    def color_range(self):
        if self.iconic_image is None:
            raise Exception("Must load test image")
        return np.amin(self.iconic_image), np.amax(self.iconic_image)

    def get_iconic_image(self):
        if self.iconic_image is None:
            raise Exception("Must load test image")
        return self.iconic_image

    def set_iconic_image(self, iconic_image_path):
        self.iconic_image_path = iconic_image_path
        self.iconic_image = cv2.imread(iconic_image_path, cv2.IMREAD_GRAYSCALE)

    def create_avg_filter(self, dimension):
        if ((dimension % 2) == 0):
            dimension +=1
        self.kernel_avg = np.ones((dimension, dimension)) / dimension**2
        return self.kernel_avg

    def create_gaussian_filter(self, dimension):
        if ((dimension % 2) == 0):
            dimension +=1
        gaus_1D = cv2.getGaussianKernel(dimension, dimension / 5, ktype=cv2.CV_32F)
        self.kernel_gaus = gaus_1D * np.transpose(gaus_1D)
        return self.kernel_gaus

    def avg_filter_convolution(self, dimension):
        if self.iconic_image is None:
            raise Exception("Must load test image")
        self.avg_image = self.manual_convolution(self.iconic_image, self.create_avg_filter(dimension))
        return self.avg_image

    def gaussian_filter_convolution(self, dimension):
        if self.iconic_image is None:
            raise Exception("Must load test image")
        self.gaussian_image = self.manual_convolution(self.iconic_image, self.create_gaussian_filter(dimension))
        return self.gaussian_image

    def median_filter_convolution(self, dimension):
        if self.iconic_image is None:
            raise Exception("Must load test image")
        self.median_image = self.median_blur(self.iconic_image, dimension, dimension)
        return self.median_image

    def bilateral_filter_convolution(self, dimension, sigmaColor, sigmaSpace):
        if self.iconic_image is None:
            raise Exception("Must load test image")
        self.bilateral_image = self.bilateral(self.iconic_image, dimension, dimension, sigmaColor, sigmaSpace)
        return self.bilateral_image






