import numpy as np
import cv2
import os

class Lab10:
    def __init__(self):
        self.video_path = None
        self.video = None
        self.faces = None


    def set_video(self, video_path):
        self.video_path = video_path
        self.video = cv2.VideoCapture(self.video_path)
        if not self.video.isOpened():
            print("Could not open video")
            raise Exception("Could not open video")

        ok, frame = self.video.read()
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        if not ok:
            print('Cannot read video file')
            raise Exception("Could not read video")
            return
        self.faces = self.face_detection(frame_gray)
        self.face_templates = []
        for i in range(0, len(self.faces)):
            self.face_templates.append(frame_gray[self.faces[i][1]:self.faces[i][1] + self.faces[i][3],
                                  self.faces[i][0]:self.faces[i][0] + self.faces[i][2]]) # model to search
        # Disegno dei rettagoli intorno alle facce
        for idx, face in enumerate(self.faces):
            cv2.rectangle(frame, (self.faces[idx][0], self.faces[idx][1]), (self.faces[idx][0] + self.faces[idx][2], self.faces[idx][1] + self.faces[idx][3]),
                          (255 - idx * 80, 0, idx * 80), 2)
        return frame

    def nextFrame(self):
        responses = []
        maxLoc = []
        if self.video is None:
            raise Exception("No opened video")
        ok, frame = self.video.read()
        if not ok:
            raise Exception("There's no next frame")
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        # Ricerca dei modelli nel frame successivo con l'NCC
        for idx, f in enumerate(self.face_templates):
            response = cv2.matchTemplate(frame_gray, f, cv2.TM_CCORR_NORMED)
            responses.append(response)
            mV, MV, mL, ML = cv2.minMaxLoc(response)
            self.faces[idx][0] = ML[0]
            self.faces[idx][1] = ML[1]
            self.faces[idx][2] = self.face_templates[idx].shape[0]
            self.faces[idx][3] = self.face_templates[idx].shape[0]
            maxLoc.append(ML)
            # Aggiornamento dell'appearence dei modelli
            self.face_templates[idx] = frame_gray[ML[1]: ML[1] + self.face_templates[idx].shape[0],
                                  ML[0]: ML[0] + self.face_templates[idx].shape[1]]

        # Disegno dei rettagoli intorno alle facce
        for idx, f in enumerate(self.face_templates):
            cv2.rectangle(frame, maxLoc[idx], (maxLoc[idx][0] + f.shape[0], maxLoc[idx][1] + f.shape[1]),
                          (255 - idx * 80, 0, idx * 80), 2)
        return frame

    def face_detection(self, image_gray):
        classifier = cv2.CascadeClassifier(os.path.dirname(os.path.abspath(__file__))+"/pre_trained_model.xml")
        self.faces = classifier.detectMultiScale(image_gray)
        return self.faces

    def stupid_tracking(self):
        while True:
            responses = []
            maxLoc = []
            ok, frame = self.video.read()
            if not ok:
                break
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            # Ricerca dei modelli nel frame successivo con l'NCC
            for idx, f in enumerate(self.face_templates):
                response = cv2.matchTemplate(frame_gray, f, cv2.TM_CCORR_NORMED)
                responses.append(response)
                mV, MV, mL, ML = cv2.minMaxLoc(response)
                maxLoc.append(ML)
                # Aggiornamento dell'appearence dei modelli
                self.face_templates[idx] = frame_gray[ML[1]: ML[1] + self.face_templates[idx].shape[0],
                                      ML[0]: ML[0] + self.face_templates[idx].shape[1]]

            # Disegno dei rettagoli intorno alle facce
            for idx, f in enumerate(self.face_templates):
                cv2.rectangle(frame, maxLoc[idx], (maxLoc[idx][0] + f.shape[0], maxLoc[idx][1] + f.shape[1]),
                              (255 - idx * 80, 0, idx * 80), 2)

            cv2.imshow("Video", frame)
            cv2.waitKey(0)
