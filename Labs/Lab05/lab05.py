import cv2
from shapely import geometry
import numpy as np

class Lab5:
    def __init__(self, sticky_image=None):
        self.sticky_image = sticky_image
        self.polygons = []
        self.vertices = []

    def get_open_polygon_vertices(self):
        return self.vertices.copy()

    def delete_last_vertex(self):
        if len(self.vertices) > 0:
            del self.vertices[-1]
        else:
            coords = list(self.polygons[-1].exterior.coords)
            self.vertices.append(geometry.Point(coords[0]))
            self.vertices.append(geometry.Point(coords[1]))
            self.vertices.append(geometry.Point(coords[2]))
            del self.polygons[-1]

    def check_convex(self, poly):
        hull = poly.convex_hull
        poly_vertices =[]
        poly_vertices.append((poly.exterior.coords.xy[0][0], poly.exterior.coords.xy[1][0]))
        poly_vertices.append((poly.exterior.coords.xy[0][1], poly.exterior.coords.xy[1][1]))
        poly_vertices.append((poly.exterior.coords.xy[0][2], poly.exterior.coords.xy[1][2]))
        poly_vertices.append((poly.exterior.coords.xy[0][3], poly.exterior.coords.xy[1][3]))

        hull_vertices = []
        hull_vertices.append((hull.exterior.coords.xy[0][0], hull.exterior.coords.xy[1][0]))
        hull_vertices.append((hull.exterior.coords.xy[0][1], hull.exterior.coords.xy[1][1]))
        hull_vertices.append((hull.exterior.coords.xy[0][2], hull.exterior.coords.xy[1][2]))
        hull_vertices.append((hull.exterior.coords.xy[0][3], hull.exterior.coords.xy[1][3]))
        for v in poly_vertices:
            if v not in hull_vertices:
                return False
        return True

    def add_vertex(self, coordinates):
        self.vertices.append(geometry.Point(coordinates))
        if len(self.vertices) ==4:
            poly = geometry.Polygon([[p.x, p.y] for p in self.vertices])
            if (self.check_convex(poly) and poly.is_valid):
                self.polygons.append(poly)
                self.vertices.clear()
                return True
            else:
                self.delete_last_vertex()
                return False
        return True

    def get_sticky_image(self):
        if self.sticky_image is None:
            raise Exception("Must load test image")
        return cv2.cvtColor(self.sticky_image, cv2.COLOR_BGR2RGB)

    def set_sticky_image(self, sticky_image_path):
        self.sticky_image_path = sticky_image_path
        self.sticky_image = cv2.imread(sticky_image_path)

    def get_background_image(self):
        if self.background_image is None:
            raise Exception("Must load test image")
        return cv2.cvtColor(self.background_image, cv2.COLOR_BGR2RGB)

    def set_background_image(self, background_image_path):
        self.background_image_path = background_image_path
        self.background_image = cv2.imread(background_image_path)

    def warp_sticky_on_background_last_polygon(self, background_image_RGB):
        background_image = cv2.cvtColor(background_image_RGB, cv2.COLOR_RGB2BGR)
        top_left_start = [0, 0]
        bottom_left_start = [0, self.sticky_image.shape[0]]
        top_right_start = [self.sticky_image.shape[1], 0]
        bottom_right_start = [self.sticky_image.shape[1], self.sticky_image.shape[0]]
        start_points = np.float32([top_left_start, bottom_left_start, bottom_right_start, top_right_start])

        last_polygon_coords = list(self.polygons[-1].exterior.coords)                      

        end_points = np.float32([last_polygon_coords[0], last_polygon_coords[1], last_polygon_coords[2], last_polygon_coords[3]])
        transformation_matrix = cv2.getPerspectiveTransform(start_points, end_points)
        warped_sticky = cv2.warpPerspective(self.sticky_image, transformation_matrix, (background_image.shape[1], background_image.shape[0]))
        new_stiched_background = background_image.copy()
        new_stiched_background[warped_sticky != (0, 0, 0)] = warped_sticky[warped_sticky != (0, 0, 0)]        
        return cv2.cvtColor(new_stiched_background, cv2.COLOR_BGR2RGB)
