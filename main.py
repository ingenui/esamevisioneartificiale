import tkinter as tk
from PIL import ImageTk, Image
import cv2
from customWidgets import TreeviewWithScrollbar, EmptyPage, DescriptionFrame
from customWidgets.Homeworks.Homework2 import Homework2Frame
from customWidgets.Homeworks.Homework3 import Homework3Frame
from customWidgets.Homeworks.Homework4 import Homework4Frame
from customWidgets.Homeworks.Homework6 import Homework6Frame
from customWidgets.Homeworks.Experiment import ExperimentFrame
from customWidgets.Labs import Lab07Frame, Lab05Frame, Lab03Frame, Lab02Frame, Lab10Frame, Lab11Frame

class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.master.columnconfigure(0, weight = 1)
        self.master.rowconfigure(0, weight = 1)

        self.columnconfigure(0, weight = 1)
        self.columnconfigure(1, weight = 99)
        self.rowconfigure(0, weight = 3)
        self.rowconfigure(1, weight = 1)

        self.grid_propagate(0)
        self.grid(sticky = "nswe")
        self.createWidgets()

    def createWidgets(self):        
        self.treeViewWithScrollbar = TreeviewWithScrollbar.TreeviewWithScrollbar(self.updatePage, self)
        self.treeViewWithScrollbar.grid(row = 0, column = 0, sticky = "nsw")

        self.currentPage = EmptyPage.EmptyPage(self)
        self.currentPage.grid(row = 0, column = 1, rowspan=2, sticky="wnse")

        self.currentDescription = DescriptionFrame.DescriptionFrame(self)
        self.currentDescription.grid(row = 1, column = 0, sticky ="nswe")

        #self.updatePage(event=None)

    def updatePage(self, event):
        itemText=""
        if len(self.treeViewWithScrollbar.treeView.selection())>0:
            item = self.treeViewWithScrollbar.treeView.selection()[0]
            itemText = self.treeViewWithScrollbar.treeView.item(item,"text")
        if itemText == "Homework 2":
            self.currentPage = Homework2Frame.Homework2Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Image noise",
                description = "Given an image, add gaussian noise with given mean and std_dev")
        elif itemText == "Homework 3":
            self.currentPage = Homework3Frame.Homework3Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Modena Skyline Segmentation",
                description="Given a Modena skyline image with Ghirlandina, segment the sky, the city and the tower"
                            " with the selected method (Canny & morphology or Kmeans)")
        elif itemText == "Homework 4":
            self.currentPage = Homework4Frame.Homework4Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Find the objects",
                description="Given an image with non-overlapped cutlery, tune the pipeline's parameters for finding separate objects ")
        elif itemText == "Homework 5":
            self.currentPage = Lab05Frame.Lab05Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Stitching",
                description="Given a background and a foreground image, draw with the mouse the stitching coordinates on the background image" \
                            " composing a quadrilateral. If you make a mistake you can press Ctrl+Z to undo.")
        elif itemText == "Homework 6":
            self.currentPage = Homework6Frame.Homework6Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Pedestrian tracking",
                description="Given a video, track pedestrians in it selecting Yolo+Sort method."
                            "Only detect them with Yolo method.")
        elif itemText == "Laboratorio 2":
            self.currentPage = Lab02Frame.Lab02Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Filtering",
                description="Given an image, compute several filtering operations and contrast stretching.")
        elif itemText == "Laboratorio 3":
            self.currentPage = Lab03Frame.Lab03Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Edges",
                description="Given an image, compute the edge images with several methods.")
        elif itemText == "Laboratorio 5":
            self.currentPage = Lab05Frame.Lab05Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Geometric transform",
                description="Given a background and a foreground image, draw with the mouse the stitching coordinates on the background image" \
                            " composing a quadrilateral. If you make a mistake you can press Ctrl+Z to undo.")
        elif itemText == "Laboratorio 7":
            self.currentPage = Lab07Frame.Lab07Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Image Retrieval",
                description="Given a folder with the images dataset and a query image, find the closest match"
                            " with the selected metric.")
        elif itemText == "Laboratorio 10":
            self.currentPage = Lab10Frame.Lab10Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Face tracking",
                description="Given a video with human faces in the first frame, track them.")
        elif itemText == "Laboratorio 11":
            self.currentPage = Lab11Frame.Lab11Frame(self)
            self.currentDescription = DescriptionFrame.DescriptionFrame(self, title="Optical flow",
                description="Compute optical flow using your laptop webcam.")
        else:
            self.currentPage = EmptyPage.EmptyPage(self)

        self.currentPage.grid(row = 0, column = 1, rowspan=2, sticky = "NSEW")
        self.currentDescription.grid(row = 1, column = 0, sticky="nswe")
        self.update_idletasks()
        self.currentDescription.titleLabel.configure(wraplength=self.currentDescription.winfo_width())
        self.currentDescription.descriptionLabel.configure(wraplength=self.currentDescription.winfo_width())

if __name__ == "__main__":
    root = tk.Tk()
    root.state("zoomed")
    root.resizable(False, False)	
    app = Application(master=root)
    app.master.title('Esame Visione Artificiale')
    app.mainloop()